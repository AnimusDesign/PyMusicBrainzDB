import os
from setuptools import setup, find_packages


setup(
    name="PyMusicBrainzDB",
    version="1.0",
    author="AnimusNull",
    author_email="sean@animus.design",
    description=("A SQLAlchemy mapping of the music brainz database."),
    license="Apache",
    keywords="musicbrainz",
    package_dir={
        '': './'},
    packages=find_packages(),
    package_data={
        '': []},
    install_requires=[
        "sqlalchemy",
        "psycopg2"],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: Apache License",
    ],
)
