# Summary

This project is a sqlalchemy mapping to the music brainz database.

# Attribution

[This is based on the upstream musicbrainz project.](https://musicbrainz.org/doc/About/Data_License)
[Guide for installing your own slave DB.](https://wiki.musicbrainz.org/Installing_MB_Slave_and_Search_Server_on_a_New_Xubuntu_Machine)
