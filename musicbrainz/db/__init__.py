# coding: utf-8
from sqlalchemy import ARRAY, Boolean, CheckConstraint, Column, Date, DateTime, Enum, ForeignKey, Index, Integer, JSON, SmallInteger, String, Table, Text, Time, text
from sqlalchemy.dialects.postgresql.base import INTERVAL, UUID
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import NullType
from sqlalchemy.ext.declarative import declarative_base
from datetime import datetime


Base = declarative_base()
metadata = Base.metadata


class AlternativeMedium(Base):
    __tablename__ = 'alternative_medium'
    __table_args__ = (
        CheckConstraint("(name)::text <> ''::text"),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('alternative_medium_id_seq'::regclass)"))
    medium = Column(Integer, nullable=False)
    alternative_release = Column(Integer, nullable=False, index=True)
    name = Column(String)


class AlternativeMediumTrack(Base):
    __tablename__ = 'alternative_medium_track'

    alternative_medium = Column(Integer, primary_key=True, nullable=False)
    track = Column(Integer, primary_key=True, nullable=False)
    alternative_track = Column(Integer, nullable=False)


class AlternativeRelease(Base):
    __tablename__ = 'alternative_release'
    __table_args__ = (
        CheckConstraint("(name)::text <> ''::text"),
        Index('alternative_release_idx_language_script', 'language', 'script')
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('alternative_release_id_seq'::regclass)"))
    gid = Column(UUID, nullable=False, unique=True)
    release = Column(Integer, nullable=False, index=True)
    name = Column(String, index=True)
    artist_credit = Column(Integer, index=True)
    type = Column(Integer, nullable=False)
    language = Column(Integer, nullable=False)
    script = Column(Integer, nullable=False)
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))


class AlternativeReleaseType(Base):
    __tablename__ = 'alternative_release_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('alternative_release_type_id_seq'::regclass)"))
    name = Column(Text, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False)


class AlternativeTrack(Base):
    __tablename__ = 'alternative_track'
    __table_args__ = (
        CheckConstraint("((name)::text <> ''::text) AND ((name IS NOT NULL) OR (artist_credit IS NOT NULL))"),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('alternative_track_id_seq'::regclass)"))
    name = Column(String, index=True)
    artist_credit = Column(Integer, index=True)
    ref_count = Column(Integer, nullable=False, server_default=text("0"))


class Annotation(Base):
    __tablename__ = 'annotation'

    id = Column(Integer, primary_key=True, server_default=text("nextval('annotation_id_seq'::regclass)"))
    editor = Column(Integer, nullable=False)
    text = Column(Text)
    changelog = Column(String(255))
    created = Column(DateTime, default=datetime.now())


class Application(Base):
    __tablename__ = 'application'

    id = Column(Integer, primary_key=True, server_default=text("nextval('application_id_seq'::regclass)"))
    owner = Column(Integer, nullable=False, index=True)
    name = Column(Text, nullable=False)
    oauth_id = Column(Text, nullable=False, unique=True)
    oauth_secret = Column(Text, nullable=False)
    oauth_redirect_uri = Column(Text)


class Area(Base):
    __tablename__ = 'area'
    __table_args__ = (
        CheckConstraint('(((end_date_year IS NOT NULL) OR (end_date_month IS NOT NULL) OR (end_date_day IS NOT NULL)) AND (ended = true)) OR ((end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL))'),
        CheckConstraint('edits_pending >= 0')
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('area_id_seq'::regclass)"))
    gid = Column(UUID, nullable=False, unique=True)
    name = Column(String, nullable=False, index=True)
    type = Column(Integer)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    begin_date_year = Column(SmallInteger)
    begin_date_month = Column(SmallInteger)
    begin_date_day = Column(SmallInteger)
    end_date_year = Column(SmallInteger)
    end_date_month = Column(SmallInteger)
    end_date_day = Column(SmallInteger)
    ended = Column(Boolean, nullable=False, server_default=text("false"))
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))


class AreaAlia(Base):
    __tablename__ = 'area_alias'
    __table_args__ = (
        CheckConstraint('(((end_date_year IS NOT NULL) OR (end_date_month IS NOT NULL) OR (end_date_day IS NOT NULL)) AND (ended = true)) OR ((end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL))'),
        CheckConstraint('((locale IS NULL) AND (primary_for_locale IS FALSE)) OR (locale IS NOT NULL)'),
        CheckConstraint('edits_pending >= 0'),
        Index('area_alias_idx_primary', 'area', 'locale', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('area_alias_id_seq'::regclass)"))
    area = Column(Integer, nullable=False, index=True)
    name = Column(String, nullable=False)
    locale = Column(Text)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    type = Column(Integer)
    sort_name = Column(String, nullable=False)
    begin_date_year = Column(SmallInteger)
    begin_date_month = Column(SmallInteger)
    begin_date_day = Column(SmallInteger)
    end_date_year = Column(SmallInteger)
    end_date_month = Column(SmallInteger)
    end_date_day = Column(SmallInteger)
    primary_for_locale = Column(Boolean, nullable=False, server_default=text("false"))
    ended = Column(Boolean, nullable=False, server_default=text("false"))


class AreaAliasType(Base):
    __tablename__ = 'area_alias_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('area_alias_type_id_seq'::regclass)"))
    name = Column(Text, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class AreaAnnotation(Base):
    __tablename__ = 'area_annotation'

    area = Column(Integer, primary_key=True, nullable=False)
    annotation = Column(Integer, primary_key=True, nullable=False)


class AreaAttribute(Base):
    __tablename__ = 'area_attribute'
    __table_args__ = (
        CheckConstraint('((area_attribute_type_allowed_value IS NULL) AND (area_attribute_text IS NOT NULL)) OR ((area_attribute_type_allowed_value IS NOT NULL) AND (area_attribute_text IS NULL))'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('area_attribute_id_seq'::regclass)"))
    area = Column(Integer, nullable=False, index=True)
    area_attribute_type = Column(Integer, nullable=False)
    area_attribute_type_allowed_value = Column(Integer)
    area_attribute_text = Column(Text)


class AreaAttributeType(Base):
    __tablename__ = 'area_attribute_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('area_attribute_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))
    free_text = Column(Boolean, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class AreaAttributeTypeAllowedValue(Base):
    __tablename__ = 'area_attribute_type_allowed_value'

    id = Column(Integer, primary_key=True, server_default=text("nextval('area_attribute_type_allowed_value_id_seq'::regclass)"))
    area_attribute_type = Column(Integer, nullable=False, index=True)
    value = Column(Text)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


t_area_containment = Table(
    'area_containment', metadata,
    Column('descendant', Integer),
    Column('parent', Integer),
    Column('type', Integer),
    Column('type_name', String(255)),
    Column('descendant_hierarchy', ARRAY(Integer))
)


class AreaGidRedirect(Base):
    __tablename__ = 'area_gid_redirect'

    gid = Column(UUID, primary_key=True)
    new_id = Column(Integer, nullable=False, index=True)
    created = Column(DateTime(True), server_default=text("now()"))


class AreaTag(Base):
    __tablename__ = 'area_tag'

    area = Column(Integer, primary_key=True, nullable=False)
    tag = Column(Integer, primary_key=True, nullable=False, index=True)
    count = Column(Integer, nullable=False)
    last_updated = Column(DateTime(True), server_default=text("now()"))


class AreaTagRaw(Base):
    __tablename__ = 'area_tag_raw'

    area = Column(Integer, primary_key=True, nullable=False, index=True)
    editor = Column(Integer, primary_key=True, nullable=False, index=True)
    tag = Column(Integer, primary_key=True, nullable=False, index=True)
    is_upvote = Column(Boolean, nullable=False, server_default=text("true"))


class AreaType(Base):
    __tablename__ = 'area_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('area_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class Artist(Base):
    __tablename__ = 'artist'
    __table_args__ = (
        CheckConstraint('(((end_date_year IS NOT NULL) OR (end_date_month IS NOT NULL) OR (end_date_day IS NOT NULL)) AND (ended = true)) OR ((end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL))'),
        CheckConstraint('edits_pending >= 0'),
        Index('artist_idx_uniq_name_comment', 'name', 'comment', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('artist_id_seq'::regclass)"))
    gid = Column(UUID, nullable=False, unique=True)
    name = Column(String, nullable=False, unique=True)
    sort_name = Column(String, nullable=False, index=True)
    begin_date_year = Column(SmallInteger)
    begin_date_month = Column(SmallInteger)
    begin_date_day = Column(SmallInteger)
    end_date_year = Column(SmallInteger)
    end_date_month = Column(SmallInteger)
    end_date_day = Column(SmallInteger)
    type = Column(Integer)
    area = Column(Integer, index=True)
    gender = Column(Integer)
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    ended = Column(Boolean, nullable=False, server_default=text("false"))
    begin_area = Column(Integer, index=True)
    end_area = Column(Integer, index=True)


class ArtistAlia(Base):
    __tablename__ = 'artist_alias'
    __table_args__ = (
        CheckConstraint('(((end_date_year IS NOT NULL) OR (end_date_month IS NOT NULL) OR (end_date_day IS NOT NULL)) AND (ended = true)) OR ((end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL))'),
        CheckConstraint('((locale IS NULL) AND (primary_for_locale IS FALSE)) OR (locale IS NOT NULL)'),
        CheckConstraint('(type <> 3) OR ((type = 3) AND ((sort_name)::text = (name)::text) AND (begin_date_year IS NULL) AND (begin_date_month IS NULL) AND (begin_date_day IS NULL) AND (end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL) AND (primary_for_locale IS FALSE) AND (locale IS NULL))'),
        CheckConstraint('edits_pending >= 0'),
        Index('artist_alias_idx_primary', 'artist', 'locale', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('artist_alias_id_seq'::regclass)"))
    artist = Column(Integer, nullable=False, index=True)
    name = Column(String, nullable=False)
    locale = Column(Text)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    type = Column(Integer)
    sort_name = Column(String, nullable=False)
    begin_date_year = Column(SmallInteger)
    begin_date_month = Column(SmallInteger)
    begin_date_day = Column(SmallInteger)
    end_date_year = Column(SmallInteger)
    end_date_month = Column(SmallInteger)
    end_date_day = Column(SmallInteger)
    primary_for_locale = Column(Boolean, nullable=False, server_default=text("false"))
    ended = Column(Boolean, nullable=False, server_default=text("false"))


class ArtistAliasType(Base):
    __tablename__ = 'artist_alias_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('artist_alias_type_id_seq'::regclass)"))
    name = Column(Text, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class ArtistAnnotation(Base):
    __tablename__ = 'artist_annotation'

    artist = Column(Integer, primary_key=True, nullable=False)
    annotation = Column(Integer, primary_key=True, nullable=False)


class ArtistAttribute(Base):
    __tablename__ = 'artist_attribute'
    __table_args__ = (
        CheckConstraint('((artist_attribute_type_allowed_value IS NULL) AND (artist_attribute_text IS NOT NULL)) OR ((artist_attribute_type_allowed_value IS NOT NULL) AND (artist_attribute_text IS NULL))'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('artist_attribute_id_seq'::regclass)"))
    artist = Column(Integer, nullable=False, index=True)
    artist_attribute_type = Column(Integer, nullable=False)
    artist_attribute_type_allowed_value = Column(Integer)
    artist_attribute_text = Column(Text)


class ArtistAttributeType(Base):
    __tablename__ = 'artist_attribute_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('artist_attribute_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))
    free_text = Column(Boolean, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class ArtistAttributeTypeAllowedValue(Base):
    __tablename__ = 'artist_attribute_type_allowed_value'

    id = Column(Integer, primary_key=True, server_default=text("nextval('artist_attribute_type_allowed_value_id_seq'::regclass)"))
    artist_attribute_type = Column(Integer, nullable=False, index=True)
    value = Column(Text)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class ArtistCredit(Base):
    __tablename__ = 'artist_credit'

    id = Column(Integer, primary_key=True, server_default=text("nextval('artist_credit_id_seq'::regclass)"))
    name = Column(String, nullable=False)
    artist_count = Column(SmallInteger, nullable=False)
    ref_count = Column(Integer, server_default=text("0"))
    created = Column(DateTime(True), server_default=text("now()"))


class ArtistCreditName(Base):
    __tablename__ = 'artist_credit_name'

    artist_credit = Column(Integer, primary_key=True, nullable=False)
    position = Column(SmallInteger, primary_key=True, nullable=False)
    artist = Column(Integer, nullable=False, index=True)
    name = Column(String, nullable=False)
    join_phrase = Column(Text, nullable=False, server_default=text("''::text"))


class ArtistGidRedirect(Base):
    __tablename__ = 'artist_gid_redirect'

    gid = Column(UUID, primary_key=True)
    new_id = Column(Integer, nullable=False, index=True)
    created = Column(DateTime(True), server_default=text("now()"))


class ArtistIpi(Base):
    __tablename__ = 'artist_ipi'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint("ipi ~ '^\\d{11}$'::text")
    )

    artist = Column(Integer, primary_key=True, nullable=False)
    ipi = Column(String(11), primary_key=True, nullable=False)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    created = Column(DateTime(True), server_default=text("now()"))


class ArtistIsni(Base):
    __tablename__ = 'artist_isni'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint("isni ~ '^\\d{15}[\\dX]$'::text")
    )

    artist = Column(Integer, primary_key=True, nullable=False)
    isni = Column(String(16), primary_key=True, nullable=False)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    created = Column(DateTime(True), server_default=text("now()"))


class ArtistMeta(Base):
    __tablename__ = 'artist_meta'
    __table_args__ = (
        CheckConstraint('(rating >= 0) AND (rating <= 100)'),
    )

    id = Column(Integer, primary_key=True)
    rating = Column(SmallInteger)
    rating_count = Column(Integer)


class ArtistRatingRaw(Base):
    __tablename__ = 'artist_rating_raw'
    __table_args__ = (
        CheckConstraint('(rating >= 0) AND (rating <= 100)'),
    )

    artist = Column(Integer, primary_key=True, nullable=False, index=True)
    editor = Column(Integer, primary_key=True, nullable=False, index=True)
    rating = Column(SmallInteger, nullable=False)


class ArtistTag(Base):
    __tablename__ = 'artist_tag'

    artist = Column(Integer, primary_key=True, nullable=False)
    tag = Column(Integer, primary_key=True, nullable=False, index=True)
    count = Column(Integer, nullable=False)
    last_updated = Column(DateTime(True), server_default=text("now()"))


class ArtistTagRaw(Base):
    __tablename__ = 'artist_tag_raw'

    artist = Column(Integer, primary_key=True, nullable=False)
    editor = Column(Integer, primary_key=True, nullable=False, index=True)
    tag = Column(Integer, primary_key=True, nullable=False, index=True)
    is_upvote = Column(Boolean, nullable=False, server_default=text("true"))


class ArtistType(Base):
    __tablename__ = 'artist_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('artist_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class AutoeditorElection(Base):
    __tablename__ = 'autoeditor_election'
    __table_args__ = (
        CheckConstraint('status = ANY (ARRAY[1, 2, 3, 4, 5, 6])'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('autoeditor_election_id_seq'::regclass)"))
    candidate = Column(Integer, nullable=False)
    proposer = Column(Integer, nullable=False)
    seconder_1 = Column(Integer)
    seconder_2 = Column(Integer)
    status = Column(Integer, nullable=False, server_default=text("1"))
    yes_votes = Column(Integer, nullable=False, server_default=text("0"))
    no_votes = Column(Integer, nullable=False, server_default=text("0"))
    propose_time = Column(DateTime(True), nullable=False, server_default=text("now()"))
    open_time = Column(DateTime(True))
    close_time = Column(DateTime(True))


class AutoeditorElectionVote(Base):
    __tablename__ = 'autoeditor_election_vote'
    __table_args__ = (
        CheckConstraint("vote = ANY (ARRAY['-1'::integer, 0, 1])"),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('autoeditor_election_vote_id_seq'::regclass)"))
    autoeditor_election = Column(Integer, nullable=False)
    voter = Column(Integer, nullable=False)
    vote = Column(Integer, nullable=False)
    vote_time = Column(DateTime(True), nullable=False, server_default=text("now()"))


class Cdtoc(Base):
    __tablename__ = 'cdtoc'

    id = Column(Integer, primary_key=True, server_default=text("nextval('cdtoc_id_seq'::regclass)"))
    discid = Column(String(28), nullable=False, unique=True)
    freedb_id = Column(String(8), nullable=False, index=True)
    track_count = Column(Integer, nullable=False)
    leadout_offset = Column(Integer, nullable=False)
    track_offset = Column(ARRAY(Integer()), nullable=False)
    degraded = Column(Boolean, nullable=False, server_default=text("false"))
    created = Column(DateTime(True), server_default=text("now()"))


class CdtocRaw(Base):
    __tablename__ = 'cdtoc_raw'
    __table_args__ = (
        Index('cdtoc_raw_toc', 'track_count', 'leadout_offset', 'track_offset', unique=True),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('cdtoc_raw_id_seq'::regclass)"))
    release = Column(Integer, nullable=False)
    discid = Column(String(28), nullable=False, index=True)
    track_count = Column(Integer, nullable=False)
    leadout_offset = Column(Integer, nullable=False)
    track_offset = Column(ARRAY(Integer()), nullable=False, index=True)


class CountryArea(Base):
    __tablename__ = 'country_area'

    area = Column(Integer, primary_key=True)


class DbmirrorPending(Base):
    __tablename__ = 'dbmirror_pending'

    seqid = Column(Integer, primary_key=True, server_default=text("nextval('dbmirror_pending_seqid_seq'::regclass)"))
    tablename = Column(String, nullable=False)
    op = Column(String(1))
    xid = Column(Integer, nullable=False, index=True)


class DbmirrorPendingdatum(Base):
    __tablename__ = 'dbmirror_pendingdata'

    seqid = Column(ForeignKey('dbmirror_pending.seqid', ondelete='CASCADE', onupdate='CASCADE'), primary_key=True, nullable=False)
    iskey = Column(Boolean, primary_key=True, nullable=False)
    data = Column(String)

    dbmirror_pending = relationship('DbmirrorPending')


class DeletedEntity(Base):
    __tablename__ = 'deleted_entity'

    gid = Column(UUID, primary_key=True)
    data = Column(JSON, nullable=False)
    deleted_at = Column(DateTime(True), nullable=False, server_default=text("now()"))


class Edit(Base):
    __tablename__ = 'edit'
    __table_args__ = (
        Index('edit_idx_editor_open_time', 'editor', 'open_time'),
        Index('edit_idx_editor_id_desc', 'editor', 'id'),
        Index('edit_idx_type_id', 'type', 'id'),
        Index('edit_idx_status_id', 'status', 'id')
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('edit_id_seq'::regclass)"))
    editor = Column(Integer, nullable=False)
    type = Column(SmallInteger, nullable=False)
    status = Column(SmallInteger, nullable=False)
    autoedit = Column(SmallInteger, nullable=False, server_default=text("0"))
    open_time = Column(DateTime(True), index=True, server_default=text("now()"))
    close_time = Column(DateTime(True), index=True)
    expire_time = Column(DateTime(True), nullable=False, index=True)
    language = Column(Integer)
    quality = Column(SmallInteger, nullable=False, server_default=text("1"))


class EditArea(Base):
    __tablename__ = 'edit_area'

    edit = Column(Integer, primary_key=True, nullable=False)
    area = Column(Integer, primary_key=True, nullable=False, index=True)


class EditArtist(Base):
    __tablename__ = 'edit_artist'

    edit = Column(Integer, primary_key=True, nullable=False)
    artist = Column(Integer, primary_key=True, nullable=False, index=True)
    status = Column(SmallInteger, nullable=False, index=True)


class EditDatum(Base):
    __tablename__ = 'edit_data'

    edit = Column(Integer, primary_key=True)
    data = Column(JSON, nullable=False)


class EditEvent(Base):
    __tablename__ = 'edit_event'

    edit = Column(Integer, primary_key=True, nullable=False)
    event = Column(Integer, primary_key=True, nullable=False, index=True)


class EditInstrument(Base):
    __tablename__ = 'edit_instrument'

    edit = Column(Integer, primary_key=True, nullable=False)
    instrument = Column(Integer, primary_key=True, nullable=False, index=True)


class EditLabel(Base):
    __tablename__ = 'edit_label'

    edit = Column(Integer, primary_key=True, nullable=False)
    label = Column(Integer, primary_key=True, nullable=False, index=True)
    status = Column(SmallInteger, nullable=False, index=True)


class EditNote(Base):
    __tablename__ = 'edit_note'
    __table_args__ = (
        Index('edit_note_idx_post_time_edit', 'post_time', 'edit'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('edit_note_id_seq'::regclass)"))
    editor = Column(Integer, nullable=False, index=True)
    edit = Column(Integer, nullable=False, index=True)
    text = Column(Text, nullable=False)
    post_time = Column(DateTime, default=datetime.now())


class EditNoteRecipient(Base):
    __tablename__ = 'edit_note_recipient'

    recipient = Column(Integer, primary_key=True, nullable=False, index=True)
    edit_note = Column(Integer, primary_key=True, nullable=False)


class EditPlace(Base):
    __tablename__ = 'edit_place'

    edit = Column(Integer, primary_key=True, nullable=False)
    place = Column(Integer, primary_key=True, nullable=False, index=True)


class EditRecording(Base):
    __tablename__ = 'edit_recording'

    edit = Column(Integer, primary_key=True, nullable=False)
    recording = Column(Integer, primary_key=True, nullable=False, index=True)


class EditRelease(Base):
    __tablename__ = 'edit_release'

    edit = Column(Integer, primary_key=True, nullable=False)
    release = Column(Integer, primary_key=True, nullable=False, index=True)


class EditReleaseGroup(Base):
    __tablename__ = 'edit_release_group'

    edit = Column(Integer, primary_key=True, nullable=False)
    release_group = Column(Integer, primary_key=True, nullable=False, index=True)


class EditSery(Base):
    __tablename__ = 'edit_series'

    edit = Column(Integer, primary_key=True, nullable=False)
    series = Column(Integer, primary_key=True, nullable=False, index=True)


class EditUrl(Base):
    __tablename__ = 'edit_url'

    edit = Column(Integer, primary_key=True, nullable=False)
    url = Column(Integer, primary_key=True, nullable=False, index=True)


class EditWork(Base):
    __tablename__ = 'edit_work'

    edit = Column(Integer, primary_key=True, nullable=False)
    work = Column(Integer, primary_key=True, nullable=False, index=True)


class Editor(Base):
    __tablename__ = 'editor'

    id = Column(Integer, primary_key=True, server_default=text("nextval('editor_id_seq'::regclass)"))
    name = Column(String(64), nullable=False)
    privs = Column(Integer, server_default=text("0"))
    email = Column(String(64), server_default=text("NULL::character varying"))
    website = Column(String(255), server_default=text("NULL::character varying"))
    bio = Column(Text)
    member_since = Column(DateTime(True), server_default=text("now()"))
    email_confirm_date = Column(DateTime(True))
    last_login_date = Column(DateTime(True), server_default=text("now()"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    birth_date = Column(Date)
    gender = Column(Integer)
    area = Column(Integer)
    password = Column(String(128), nullable=False)
    ha1 = Column(String(32), nullable=False)
    deleted = Column(Boolean, nullable=False, server_default=text("false"))


class EditorCollection(Base):
    __tablename__ = 'editor_collection'

    id = Column(Integer, primary_key=True, server_default=text("nextval('editor_collection_id_seq'::regclass)"))
    gid = Column(UUID, nullable=False, unique=True)
    editor = Column(Integer, nullable=False, index=True)
    name = Column(String, nullable=False, index=True)
    public = Column(Boolean, nullable=False, server_default=text("false"))
    description = Column(Text, nullable=False, server_default=text("''::text"))
    type = Column(Integer, nullable=False)


class EditorCollectionArea(Base):
    __tablename__ = 'editor_collection_area'

    collection = Column(Integer, primary_key=True, nullable=False)
    area = Column(Integer, primary_key=True, nullable=False)


class EditorCollectionArtist(Base):
    __tablename__ = 'editor_collection_artist'

    collection = Column(Integer, primary_key=True, nullable=False)
    artist = Column(Integer, primary_key=True, nullable=False)


class EditorCollectionDeletedEntity(Base):
    __tablename__ = 'editor_collection_deleted_entity'

    collection = Column(Integer, primary_key=True, nullable=False)
    gid = Column(UUID, primary_key=True, nullable=False)


class EditorCollectionEvent(Base):
    __tablename__ = 'editor_collection_event'

    collection = Column(Integer, primary_key=True, nullable=False)
    event = Column(Integer, primary_key=True, nullable=False)


class EditorCollectionInstrument(Base):
    __tablename__ = 'editor_collection_instrument'

    collection = Column(Integer, primary_key=True, nullable=False)
    instrument = Column(Integer, primary_key=True, nullable=False)


class EditorCollectionLabel(Base):
    __tablename__ = 'editor_collection_label'

    collection = Column(Integer, primary_key=True, nullable=False)
    label = Column(Integer, primary_key=True, nullable=False)


class EditorCollectionPlace(Base):
    __tablename__ = 'editor_collection_place'

    collection = Column(Integer, primary_key=True, nullable=False)
    place = Column(Integer, primary_key=True, nullable=False)


class EditorCollectionRecording(Base):
    __tablename__ = 'editor_collection_recording'

    collection = Column(Integer, primary_key=True, nullable=False)
    recording = Column(Integer, primary_key=True, nullable=False)


class EditorCollectionRelease(Base):
    __tablename__ = 'editor_collection_release'

    collection = Column(Integer, primary_key=True, nullable=False)
    release = Column(Integer, primary_key=True, nullable=False)


class EditorCollectionReleaseGroup(Base):
    __tablename__ = 'editor_collection_release_group'

    collection = Column(Integer, primary_key=True, nullable=False)
    release_group = Column(Integer, primary_key=True, nullable=False)


class EditorCollectionSery(Base):
    __tablename__ = 'editor_collection_series'

    collection = Column(Integer, primary_key=True, nullable=False)
    series = Column(Integer, primary_key=True, nullable=False)


class EditorCollectionType(Base):
    __tablename__ = 'editor_collection_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('editor_collection_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    entity_type = Column(String(50), nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class EditorCollectionWork(Base):
    __tablename__ = 'editor_collection_work'

    collection = Column(Integer, primary_key=True, nullable=False)
    work = Column(Integer, primary_key=True, nullable=False)


class EditorLanguage(Base):
    __tablename__ = 'editor_language'

    editor = Column(Integer, primary_key=True, nullable=False)
    language = Column(Integer, primary_key=True, nullable=False, index=True)
    fluency = Column(Enum('basic', 'intermediate', 'advanced', 'native', name='fluency'), nullable=False)


class EditorOauthToken(Base):
    __tablename__ = 'editor_oauth_token'

    id = Column(Integer, primary_key=True, server_default=text("nextval('editor_oauth_token_id_seq'::regclass)"))
    editor = Column(Integer, nullable=False, index=True)
    application = Column(Integer, nullable=False)
    authorization_code = Column(Text)
    refresh_token = Column(Text, unique=True)
    access_token = Column(Text, unique=True)
    expire_time = Column(DateTime(True), nullable=False)
    scope = Column(Integer, nullable=False, server_default=text("0"))
    granted = Column(DateTime(True), nullable=False, server_default=text("now()"))


class EditorPreference(Base):
    __tablename__ = 'editor_preference'
    __table_args__ = (
        Index('editor_preference_idx_editor_name', 'editor', 'name', unique=True),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('editor_preference_id_seq'::regclass)"))
    editor = Column(Integer, nullable=False)
    name = Column(String(50), nullable=False)
    value = Column(String(100), nullable=False)


class EditorSubscribeArtist(Base):
    __tablename__ = 'editor_subscribe_artist'
    __table_args__ = (
        Index('editor_subscribe_artist_idx_uniq', 'editor', 'artist', unique=True),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('editor_subscribe_artist_id_seq'::regclass)"))
    editor = Column(Integer, nullable=False)
    artist = Column(Integer, nullable=False, index=True)
    last_edit_sent = Column(Integer, nullable=False)


class EditorSubscribeArtistDeleted(Base):
    __tablename__ = 'editor_subscribe_artist_deleted'

    editor = Column(Integer, primary_key=True, nullable=False)
    gid = Column(UUID, primary_key=True, nullable=False)
    deleted_by = Column(Integer, nullable=False)


class EditorSubscribeCollection(Base):
    __tablename__ = 'editor_subscribe_collection'
    __table_args__ = (
        Index('editor_subscribe_collection_idx_uniq', 'editor', 'collection', unique=True),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('editor_subscribe_collection_id_seq'::regclass)"))
    editor = Column(Integer, nullable=False)
    collection = Column(Integer, nullable=False, index=True)
    last_edit_sent = Column(Integer, nullable=False)
    available = Column(Boolean, nullable=False, server_default=text("true"))
    last_seen_name = Column(String(255))


class EditorSubscribeEditor(Base):
    __tablename__ = 'editor_subscribe_editor'
    __table_args__ = (
        Index('editor_subscribe_editor_idx_uniq', 'editor', 'subscribed_editor', unique=True),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('editor_subscribe_editor_id_seq'::regclass)"))
    editor = Column(Integer, nullable=False)
    subscribed_editor = Column(Integer, nullable=False)
    last_edit_sent = Column(Integer, nullable=False)


class EditorSubscribeLabel(Base):
    __tablename__ = 'editor_subscribe_label'
    __table_args__ = (
        Index('editor_subscribe_label_idx_uniq', 'editor', 'label', unique=True),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('editor_subscribe_label_id_seq'::regclass)"))
    editor = Column(Integer, nullable=False)
    label = Column(Integer, nullable=False, index=True)
    last_edit_sent = Column(Integer, nullable=False)


class EditorSubscribeLabelDeleted(Base):
    __tablename__ = 'editor_subscribe_label_deleted'

    editor = Column(Integer, primary_key=True, nullable=False)
    gid = Column(UUID, primary_key=True, nullable=False)
    deleted_by = Column(Integer, nullable=False)


class EditorSubscribeSery(Base):
    __tablename__ = 'editor_subscribe_series'
    __table_args__ = (
        Index('editor_subscribe_series_idx_uniq', 'editor', 'series', unique=True),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('editor_subscribe_series_id_seq'::regclass)"))
    editor = Column(Integer, nullable=False)
    series = Column(Integer, nullable=False, index=True)
    last_edit_sent = Column(Integer, nullable=False)


class EditorSubscribeSeriesDeleted(Base):
    __tablename__ = 'editor_subscribe_series_deleted'

    editor = Column(Integer, primary_key=True, nullable=False)
    gid = Column(UUID, primary_key=True, nullable=False)
    deleted_by = Column(Integer, nullable=False)


class EditorWatchArtist(Base):
    __tablename__ = 'editor_watch_artist'

    artist = Column(Integer, primary_key=True, nullable=False)
    editor = Column(Integer, primary_key=True, nullable=False)


class EditorWatchPreference(Base):
    __tablename__ = 'editor_watch_preferences'

    editor = Column(Integer, primary_key=True)
    notify_via_email = Column(Boolean, nullable=False, server_default=text("true"))
    notification_timeframe = Column(INTERVAL, nullable=False, server_default=text("'7 days'::interval"))
    last_checked = Column(DateTime(True), nullable=False, server_default=text("now()"))


class EditorWatchReleaseGroupType(Base):
    __tablename__ = 'editor_watch_release_group_type'

    editor = Column(Integer, primary_key=True, nullable=False)
    release_group_type = Column(Integer, primary_key=True, nullable=False)


class EditorWatchReleaseStatu(Base):
    __tablename__ = 'editor_watch_release_status'

    editor = Column(Integer, primary_key=True, nullable=False)
    release_status = Column(Integer, primary_key=True, nullable=False)


class Event(Base):
    __tablename__ = 'event'
    __table_args__ = (
        CheckConstraint('(((end_date_year IS NOT NULL) OR (end_date_month IS NOT NULL) OR (end_date_day IS NOT NULL)) AND (ended = true)) OR ((end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL))'),
        CheckConstraint('edits_pending >= 0')
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('event_id_seq'::regclass)"))
    gid = Column(UUID, nullable=False, unique=True)
    name = Column(String, nullable=False, index=True)
    begin_date_year = Column(SmallInteger)
    begin_date_month = Column(SmallInteger)
    begin_date_day = Column(SmallInteger)
    end_date_year = Column(SmallInteger)
    end_date_month = Column(SmallInteger)
    end_date_day = Column(SmallInteger)
    time = Column(Time)
    type = Column(Integer)
    cancelled = Column(Boolean, nullable=False, server_default=text("false"))
    setlist = Column(Text)
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    ended = Column(Boolean, nullable=False, server_default=text("false"))


class EventAlia(Base):
    __tablename__ = 'event_alias'
    __table_args__ = (
        CheckConstraint('(((end_date_year IS NOT NULL) OR (end_date_month IS NOT NULL) OR (end_date_day IS NOT NULL)) AND (ended = true)) OR ((end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL))'),
        CheckConstraint('((locale IS NULL) AND (primary_for_locale IS FALSE)) OR (locale IS NOT NULL)'),
        CheckConstraint('(type <> 2) OR ((type = 2) AND ((sort_name)::text = (name)::text) AND (begin_date_year IS NULL) AND (begin_date_month IS NULL) AND (begin_date_day IS NULL) AND (end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL) AND (primary_for_locale IS FALSE) AND (locale IS NULL))'),
        CheckConstraint('edits_pending >= 0'),
        Index('event_alias_idx_primary', 'event', 'locale', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('event_alias_id_seq'::regclass)"))
    event = Column(Integer, nullable=False, index=True)
    name = Column(String, nullable=False)
    locale = Column(Text)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    type = Column(Integer)
    sort_name = Column(String, nullable=False)
    begin_date_year = Column(SmallInteger)
    begin_date_month = Column(SmallInteger)
    begin_date_day = Column(SmallInteger)
    end_date_year = Column(SmallInteger)
    end_date_month = Column(SmallInteger)
    end_date_day = Column(SmallInteger)
    primary_for_locale = Column(Boolean, nullable=False, server_default=text("false"))
    ended = Column(Boolean, nullable=False, server_default=text("false"))


class EventAliasType(Base):
    __tablename__ = 'event_alias_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('event_alias_type_id_seq'::regclass)"))
    name = Column(Text, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class EventAnnotation(Base):
    __tablename__ = 'event_annotation'

    event = Column(Integer, primary_key=True, nullable=False)
    annotation = Column(Integer, primary_key=True, nullable=False)


class EventAttribute(Base):
    __tablename__ = 'event_attribute'
    __table_args__ = (
        CheckConstraint('((event_attribute_type_allowed_value IS NULL) AND (event_attribute_text IS NOT NULL)) OR ((event_attribute_type_allowed_value IS NOT NULL) AND (event_attribute_text IS NULL))'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('event_attribute_id_seq'::regclass)"))
    event = Column(Integer, nullable=False, index=True)
    event_attribute_type = Column(Integer, nullable=False)
    event_attribute_type_allowed_value = Column(Integer)
    event_attribute_text = Column(Text)


class EventAttributeType(Base):
    __tablename__ = 'event_attribute_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('event_attribute_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))
    free_text = Column(Boolean, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class EventAttributeTypeAllowedValue(Base):
    __tablename__ = 'event_attribute_type_allowed_value'

    id = Column(Integer, primary_key=True, server_default=text("nextval('event_attribute_type_allowed_value_id_seq'::regclass)"))
    event_attribute_type = Column(Integer, nullable=False, index=True)
    value = Column(Text)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class EventGidRedirect(Base):
    __tablename__ = 'event_gid_redirect'

    gid = Column(UUID, primary_key=True)
    new_id = Column(Integer, nullable=False, index=True)
    created = Column(DateTime(True), server_default=text("now()"))


class EventMeta(Base):
    __tablename__ = 'event_meta'
    __table_args__ = (
        CheckConstraint('(rating >= 0) AND (rating <= 100)'),
    )

    id = Column(Integer, primary_key=True)
    rating = Column(SmallInteger)
    rating_count = Column(Integer)


class EventRatingRaw(Base):
    __tablename__ = 'event_rating_raw'
    __table_args__ = (
        CheckConstraint('(rating >= 0) AND (rating <= 100)'),
    )

    event = Column(Integer, primary_key=True, nullable=False, index=True)
    editor = Column(Integer, primary_key=True, nullable=False, index=True)
    rating = Column(SmallInteger, nullable=False)


t_event_series = Table(
    'event_series', metadata,
    Column('event', Integer),
    Column('series', Integer),
    Column('relationship', Integer),
    Column('link_order', Integer),
    Column('link', Integer),
    Column('text_value', Text)
)


class EventTag(Base):
    __tablename__ = 'event_tag'

    event = Column(Integer, primary_key=True, nullable=False)
    tag = Column(Integer, primary_key=True, nullable=False, index=True)
    count = Column(Integer, nullable=False)
    last_updated = Column(DateTime(True), server_default=text("now()"))


class EventTagRaw(Base):
    __tablename__ = 'event_tag_raw'

    event = Column(Integer, primary_key=True, nullable=False)
    editor = Column(Integer, primary_key=True, nullable=False, index=True)
    tag = Column(Integer, primary_key=True, nullable=False, index=True)
    is_upvote = Column(Boolean, nullable=False, server_default=text("true"))


class EventType(Base):
    __tablename__ = 'event_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('event_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class Gender(Base):
    __tablename__ = 'gender'

    id = Column(Integer, primary_key=True, server_default=text("nextval('gender_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class Instrument(Base):
    __tablename__ = 'instrument'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('instrument_id_seq'::regclass)"))
    gid = Column(UUID, nullable=False, unique=True)
    name = Column(String, nullable=False, index=True)
    type = Column(Integer)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))
    description = Column(Text, nullable=False, server_default=text("''::text"))


class InstrumentAlia(Base):
    __tablename__ = 'instrument_alias'
    __table_args__ = (
        CheckConstraint('(((end_date_year IS NOT NULL) OR (end_date_month IS NOT NULL) OR (end_date_day IS NOT NULL)) AND (ended = true)) OR ((end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL))'),
        CheckConstraint('((locale IS NULL) AND (primary_for_locale IS FALSE)) OR (locale IS NOT NULL)'),
        CheckConstraint('(type <> 2) OR ((type = 2) AND ((sort_name)::text = (name)::text) AND (begin_date_year IS NULL) AND (begin_date_month IS NULL) AND (begin_date_day IS NULL) AND (end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL) AND (primary_for_locale IS FALSE) AND (locale IS NULL))'),
        CheckConstraint('edits_pending >= 0'),
        Index('instrument_alias_idx_primary', 'instrument', 'locale', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('instrument_alias_id_seq'::regclass)"))
    instrument = Column(Integer, nullable=False, index=True)
    name = Column(String, nullable=False)
    locale = Column(Text)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    type = Column(Integer)
    sort_name = Column(String, nullable=False)
    begin_date_year = Column(SmallInteger)
    begin_date_month = Column(SmallInteger)
    begin_date_day = Column(SmallInteger)
    end_date_year = Column(SmallInteger)
    end_date_month = Column(SmallInteger)
    end_date_day = Column(SmallInteger)
    primary_for_locale = Column(Boolean, nullable=False, server_default=text("false"))
    ended = Column(Boolean, nullable=False, server_default=text("false"))


class InstrumentAliasType(Base):
    __tablename__ = 'instrument_alias_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('instrument_alias_type_id_seq'::regclass)"))
    name = Column(Text, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class InstrumentAnnotation(Base):
    __tablename__ = 'instrument_annotation'

    instrument = Column(Integer, primary_key=True, nullable=False)
    annotation = Column(Integer, primary_key=True, nullable=False)


class InstrumentAttribute(Base):
    __tablename__ = 'instrument_attribute'
    __table_args__ = (
        CheckConstraint('((instrument_attribute_type_allowed_value IS NULL) AND (instrument_attribute_text IS NOT NULL)) OR ((instrument_attribute_type_allowed_value IS NOT NULL) AND (instrument_attribute_text IS NULL))'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('instrument_attribute_id_seq'::regclass)"))
    instrument = Column(Integer, nullable=False, index=True)
    instrument_attribute_type = Column(Integer, nullable=False)
    instrument_attribute_type_allowed_value = Column(Integer)
    instrument_attribute_text = Column(Text)


class InstrumentAttributeType(Base):
    __tablename__ = 'instrument_attribute_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('instrument_attribute_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))
    free_text = Column(Boolean, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class InstrumentAttributeTypeAllowedValue(Base):
    __tablename__ = 'instrument_attribute_type_allowed_value'

    id = Column(Integer, primary_key=True, server_default=text("nextval('instrument_attribute_type_allowed_value_id_seq'::regclass)"))
    instrument_attribute_type = Column(Integer, nullable=False, index=True)
    value = Column(Text)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class InstrumentGidRedirect(Base):
    __tablename__ = 'instrument_gid_redirect'

    gid = Column(UUID, primary_key=True)
    new_id = Column(Integer, nullable=False, index=True)
    created = Column(DateTime(True), server_default=text("now()"))


class InstrumentTag(Base):
    __tablename__ = 'instrument_tag'

    instrument = Column(Integer, primary_key=True, nullable=False)
    tag = Column(Integer, primary_key=True, nullable=False, index=True)
    count = Column(Integer, nullable=False)
    last_updated = Column(DateTime(True), server_default=text("now()"))


class InstrumentTagRaw(Base):
    __tablename__ = 'instrument_tag_raw'

    instrument = Column(Integer, primary_key=True, nullable=False, index=True)
    editor = Column(Integer, primary_key=True, nullable=False, index=True)
    tag = Column(Integer, primary_key=True, nullable=False, index=True)
    is_upvote = Column(Boolean, nullable=False, server_default=text("true"))


class InstrumentType(Base):
    __tablename__ = 'instrument_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('instrument_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class Iso31661(Base):
    __tablename__ = 'iso_3166_1'

    area = Column(Integer, nullable=False, index=True)
    code = Column(String(2), primary_key=True)


class Iso31662(Base):
    __tablename__ = 'iso_3166_2'

    area = Column(Integer, nullable=False, index=True)
    code = Column(String(10), primary_key=True)


class Iso31663(Base):
    __tablename__ = 'iso_3166_3'

    area = Column(Integer, nullable=False, index=True)
    code = Column(String(4), primary_key=True)


class Isrc(Base):
    __tablename__ = 'isrc'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint("isrc ~ '^[A-Z]{2}[A-Z0-9]{3}[0-9]{7}$'::text"),
        Index('isrc_idx_isrc_recording', 'isrc', 'recording', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('isrc_id_seq'::regclass)"))
    recording = Column(Integer, nullable=False, index=True)
    isrc = Column(String(12), nullable=False, index=True)
    source = Column(SmallInteger)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    created = Column(DateTime(True), server_default=text("now()"))


class Iswc(Base):
    __tablename__ = 'iswc'
    __table_args__ = (
        CheckConstraint("iswc ~ '^T-?\\d{3}.?\\d{3}.?\\d{3}[-.]?\\d$'::text"),
        Index('iswc_idx_iswc', 'iswc', 'work', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('iswc_id_seq'::regclass)"))
    work = Column(Integer, nullable=False, index=True)
    iswc = Column(String(15))
    source = Column(SmallInteger)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    created = Column(DateTime(True), nullable=False, server_default=text("now()"))


class LAreaArea(Base):
    __tablename__ = 'l_area_area'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_area_area_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_area_area_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LAreaArtist(Base):
    __tablename__ = 'l_area_artist'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_area_artist_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_area_artist_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LAreaEvent(Base):
    __tablename__ = 'l_area_event'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_area_event_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_area_event_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LAreaInstrument(Base):
    __tablename__ = 'l_area_instrument'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_area_instrument_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_area_instrument_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LAreaLabel(Base):
    __tablename__ = 'l_area_label'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_area_label_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_area_label_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LAreaPlace(Base):
    __tablename__ = 'l_area_place'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_area_place_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_area_place_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LAreaRecording(Base):
    __tablename__ = 'l_area_recording'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_area_recording_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_area_recording_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LAreaRelease(Base):
    __tablename__ = 'l_area_release'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_area_release_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_area_release_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LAreaReleaseGroup(Base):
    __tablename__ = 'l_area_release_group'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_area_release_group_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_area_release_group_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LAreaSery(Base):
    __tablename__ = 'l_area_series'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_area_series_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_area_series_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LAreaUrl(Base):
    __tablename__ = 'l_area_url'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_area_url_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_area_url_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LAreaWork(Base):
    __tablename__ = 'l_area_work'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_area_work_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_area_work_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LArtistArtist(Base):
    __tablename__ = 'l_artist_artist'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_artist_artist_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_artist_artist_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LArtistEvent(Base):
    __tablename__ = 'l_artist_event'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_artist_event_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_artist_event_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LArtistInstrument(Base):
    __tablename__ = 'l_artist_instrument'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_artist_instrument_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_artist_instrument_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LArtistLabel(Base):
    __tablename__ = 'l_artist_label'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_artist_label_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_artist_label_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LArtistPlace(Base):
    __tablename__ = 'l_artist_place'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_artist_place_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_artist_place_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LArtistRecording(Base):
    __tablename__ = 'l_artist_recording'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_artist_recording_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_artist_recording_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LArtistRelease(Base):
    __tablename__ = 'l_artist_release'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_artist_release_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_artist_release_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LArtistReleaseGroup(Base):
    __tablename__ = 'l_artist_release_group'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_artist_release_group_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_artist_release_group_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LArtistSery(Base):
    __tablename__ = 'l_artist_series'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_artist_series_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_artist_series_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LArtistUrl(Base):
    __tablename__ = 'l_artist_url'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_artist_url_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_artist_url_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LArtistWork(Base):
    __tablename__ = 'l_artist_work'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_artist_work_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_artist_work_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LEventEvent(Base):
    __tablename__ = 'l_event_event'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_event_event_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_event_event_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LEventInstrument(Base):
    __tablename__ = 'l_event_instrument'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_event_instrument_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_event_instrument_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LEventLabel(Base):
    __tablename__ = 'l_event_label'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_event_label_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_event_label_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LEventPlace(Base):
    __tablename__ = 'l_event_place'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_event_place_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_event_place_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LEventRecording(Base):
    __tablename__ = 'l_event_recording'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_event_recording_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_event_recording_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LEventRelease(Base):
    __tablename__ = 'l_event_release'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_event_release_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_event_release_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LEventReleaseGroup(Base):
    __tablename__ = 'l_event_release_group'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_event_release_group_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_event_release_group_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LEventSery(Base):
    __tablename__ = 'l_event_series'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_event_series_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_event_series_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LEventUrl(Base):
    __tablename__ = 'l_event_url'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_event_url_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_event_url_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LEventWork(Base):
    __tablename__ = 'l_event_work'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_event_work_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_event_work_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LInstrumentInstrument(Base):
    __tablename__ = 'l_instrument_instrument'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_instrument_instrument_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_instrument_instrument_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LInstrumentLabel(Base):
    __tablename__ = 'l_instrument_label'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_instrument_label_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_instrument_label_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LInstrumentPlace(Base):
    __tablename__ = 'l_instrument_place'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_instrument_place_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_instrument_place_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LInstrumentRecording(Base):
    __tablename__ = 'l_instrument_recording'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_instrument_recording_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_instrument_recording_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LInstrumentRelease(Base):
    __tablename__ = 'l_instrument_release'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_instrument_release_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_instrument_release_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LInstrumentReleaseGroup(Base):
    __tablename__ = 'l_instrument_release_group'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_instrument_release_group_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_instrument_release_group_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LInstrumentSery(Base):
    __tablename__ = 'l_instrument_series'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_instrument_series_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_instrument_series_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LInstrumentUrl(Base):
    __tablename__ = 'l_instrument_url'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_instrument_url_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_instrument_url_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LInstrumentWork(Base):
    __tablename__ = 'l_instrument_work'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_instrument_work_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_instrument_work_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LLabelLabel(Base):
    __tablename__ = 'l_label_label'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_label_label_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_label_label_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LLabelPlace(Base):
    __tablename__ = 'l_label_place'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_label_place_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_label_place_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LLabelRecording(Base):
    __tablename__ = 'l_label_recording'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_label_recording_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_label_recording_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LLabelRelease(Base):
    __tablename__ = 'l_label_release'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_label_release_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_label_release_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LLabelReleaseGroup(Base):
    __tablename__ = 'l_label_release_group'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_label_release_group_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_label_release_group_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LLabelSery(Base):
    __tablename__ = 'l_label_series'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_label_series_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_label_series_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LLabelUrl(Base):
    __tablename__ = 'l_label_url'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_label_url_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_label_url_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LLabelWork(Base):
    __tablename__ = 'l_label_work'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_label_work_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_label_work_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LPlacePlace(Base):
    __tablename__ = 'l_place_place'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_place_place_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_place_place_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LPlaceRecording(Base):
    __tablename__ = 'l_place_recording'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_place_recording_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_place_recording_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LPlaceRelease(Base):
    __tablename__ = 'l_place_release'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_place_release_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_place_release_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LPlaceReleaseGroup(Base):
    __tablename__ = 'l_place_release_group'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_place_release_group_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_place_release_group_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LPlaceSery(Base):
    __tablename__ = 'l_place_series'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_place_series_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_place_series_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LPlaceUrl(Base):
    __tablename__ = 'l_place_url'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_place_url_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_place_url_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LPlaceWork(Base):
    __tablename__ = 'l_place_work'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_place_work_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_place_work_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LRecordingRecording(Base):
    __tablename__ = 'l_recording_recording'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_recording_recording_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_recording_recording_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LRecordingRelease(Base):
    __tablename__ = 'l_recording_release'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_recording_release_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_recording_release_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LRecordingReleaseGroup(Base):
    __tablename__ = 'l_recording_release_group'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_recording_release_group_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_recording_release_group_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LRecordingSery(Base):
    __tablename__ = 'l_recording_series'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_recording_series_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_recording_series_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LRecordingUrl(Base):
    __tablename__ = 'l_recording_url'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_recording_url_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_recording_url_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LRecordingWork(Base):
    __tablename__ = 'l_recording_work'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_recording_work_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_recording_work_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LReleaseGroupReleaseGroup(Base):
    __tablename__ = 'l_release_group_release_group'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_release_group_release_group_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_release_group_release_group_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LReleaseGroupSery(Base):
    __tablename__ = 'l_release_group_series'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_release_group_series_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_release_group_series_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LReleaseGroupUrl(Base):
    __tablename__ = 'l_release_group_url'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_release_group_url_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_release_group_url_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LReleaseGroupWork(Base):
    __tablename__ = 'l_release_group_work'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_release_group_work_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_release_group_work_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LReleaseRelease(Base):
    __tablename__ = 'l_release_release'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_release_release_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_release_release_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LReleaseReleaseGroup(Base):
    __tablename__ = 'l_release_release_group'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_release_release_group_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_release_release_group_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LReleaseSery(Base):
    __tablename__ = 'l_release_series'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_release_series_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_release_series_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LReleaseUrl(Base):
    __tablename__ = 'l_release_url'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_release_url_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_release_url_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LReleaseWork(Base):
    __tablename__ = 'l_release_work'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_release_work_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_release_work_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LSeriesSery(Base):
    __tablename__ = 'l_series_series'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_series_series_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_series_series_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LSeriesUrl(Base):
    __tablename__ = 'l_series_url'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_series_url_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_series_url_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LSeriesWork(Base):
    __tablename__ = 'l_series_work'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_series_work_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_series_work_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LUrlUrl(Base):
    __tablename__ = 'l_url_url'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_url_url_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_url_url_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LUrlWork(Base):
    __tablename__ = 'l_url_work'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_url_work_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_url_work_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class LWorkWork(Base):
    __tablename__ = 'l_work_work'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint('link_order >= 0'),
        Index('l_work_work_idx_uniq', 'entity0', 'entity1', 'link', 'link_order', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('l_work_work_id_seq'::regclass)"))
    link = Column(Integer, nullable=False)
    entity0 = Column(Integer, nullable=False)
    entity1 = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    link_order = Column(Integer, nullable=False, server_default=text("0"))
    entity0_credit = Column(Text, nullable=False, server_default=text("''::text"))
    entity1_credit = Column(Text, nullable=False, server_default=text("''::text"))


class Label(Base):
    __tablename__ = 'label'
    __table_args__ = (
        CheckConstraint('(((end_date_year IS NOT NULL) OR (end_date_month IS NOT NULL) OR (end_date_day IS NOT NULL)) AND (ended = true)) OR ((end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL))'),
        CheckConstraint('(label_code > 0) AND (label_code < 100000)'),
        CheckConstraint('edits_pending >= 0'),
        Index('label_idx_uniq_name_comment', 'name', 'comment', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('label_id_seq'::regclass)"))
    gid = Column(UUID, nullable=False, unique=True)
    name = Column(String, nullable=False, unique=True)
    begin_date_year = Column(SmallInteger)
    begin_date_month = Column(SmallInteger)
    begin_date_day = Column(SmallInteger)
    end_date_year = Column(SmallInteger)
    end_date_month = Column(SmallInteger)
    end_date_day = Column(SmallInteger)
    label_code = Column(Integer)
    type = Column(Integer)
    area = Column(Integer, index=True)
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    ended = Column(Boolean, nullable=False, server_default=text("false"))


class LabelAlia(Base):
    __tablename__ = 'label_alias'
    __table_args__ = (
        CheckConstraint('(((end_date_year IS NOT NULL) OR (end_date_month IS NOT NULL) OR (end_date_day IS NOT NULL)) AND (ended = true)) OR ((end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL))'),
        CheckConstraint('((locale IS NULL) AND (primary_for_locale IS FALSE)) OR (locale IS NOT NULL)'),
        CheckConstraint('(type <> 2) OR ((type = 2) AND ((sort_name)::text = (name)::text) AND (begin_date_year IS NULL) AND (begin_date_month IS NULL) AND (begin_date_day IS NULL) AND (end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL) AND (primary_for_locale IS FALSE) AND (locale IS NULL))'),
        CheckConstraint('edits_pending >= 0'),
        Index('label_alias_idx_primary', 'label', 'locale', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('label_alias_id_seq'::regclass)"))
    label = Column(Integer, nullable=False, index=True)
    name = Column(String, nullable=False)
    locale = Column(Text)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    type = Column(Integer)
    sort_name = Column(String, nullable=False)
    begin_date_year = Column(SmallInteger)
    begin_date_month = Column(SmallInteger)
    begin_date_day = Column(SmallInteger)
    end_date_year = Column(SmallInteger)
    end_date_month = Column(SmallInteger)
    end_date_day = Column(SmallInteger)
    primary_for_locale = Column(Boolean, nullable=False, server_default=text("false"))
    ended = Column(Boolean, nullable=False, server_default=text("false"))


class LabelAliasType(Base):
    __tablename__ = 'label_alias_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('label_alias_type_id_seq'::regclass)"))
    name = Column(Text, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class LabelAnnotation(Base):
    __tablename__ = 'label_annotation'

    label = Column(Integer, primary_key=True, nullable=False)
    annotation = Column(Integer, primary_key=True, nullable=False)


class LabelAttribute(Base):
    __tablename__ = 'label_attribute'
    __table_args__ = (
        CheckConstraint('((label_attribute_type_allowed_value IS NULL) AND (label_attribute_text IS NOT NULL)) OR ((label_attribute_type_allowed_value IS NOT NULL) AND (label_attribute_text IS NULL))'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('label_attribute_id_seq'::regclass)"))
    label = Column(Integer, nullable=False, index=True)
    label_attribute_type = Column(Integer, nullable=False)
    label_attribute_type_allowed_value = Column(Integer)
    label_attribute_text = Column(Text)


class LabelAttributeType(Base):
    __tablename__ = 'label_attribute_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('label_attribute_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))
    free_text = Column(Boolean, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class LabelAttributeTypeAllowedValue(Base):
    __tablename__ = 'label_attribute_type_allowed_value'

    id = Column(Integer, primary_key=True, server_default=text("nextval('label_attribute_type_allowed_value_id_seq'::regclass)"))
    label_attribute_type = Column(Integer, nullable=False, index=True)
    value = Column(Text)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class LabelGidRedirect(Base):
    __tablename__ = 'label_gid_redirect'

    gid = Column(UUID, primary_key=True)
    new_id = Column(Integer, nullable=False, index=True)
    created = Column(DateTime(True), server_default=text("now()"))


class LabelIpi(Base):
    __tablename__ = 'label_ipi'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint("ipi ~ '^\\d{11}$'::text")
    )

    label = Column(Integer, primary_key=True, nullable=False)
    ipi = Column(String(11), primary_key=True, nullable=False)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    created = Column(DateTime(True), server_default=text("now()"))


class LabelIsni(Base):
    __tablename__ = 'label_isni'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        CheckConstraint("isni ~ '^\\d{15}[\\dX]$'::text")
    )

    label = Column(Integer, primary_key=True, nullable=False)
    isni = Column(String(16), primary_key=True, nullable=False)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    created = Column(DateTime(True), server_default=text("now()"))


class LabelMeta(Base):
    __tablename__ = 'label_meta'
    __table_args__ = (
        CheckConstraint('(rating >= 0) AND (rating <= 100)'),
    )

    id = Column(Integer, primary_key=True)
    rating = Column(SmallInteger)
    rating_count = Column(Integer)


class LabelRatingRaw(Base):
    __tablename__ = 'label_rating_raw'
    __table_args__ = (
        CheckConstraint('(rating >= 0) AND (rating <= 100)'),
    )

    label = Column(Integer, primary_key=True, nullable=False, index=True)
    editor = Column(Integer, primary_key=True, nullable=False, index=True)
    rating = Column(SmallInteger, nullable=False)


class LabelTag(Base):
    __tablename__ = 'label_tag'

    label = Column(Integer, primary_key=True, nullable=False)
    tag = Column(Integer, primary_key=True, nullable=False, index=True)
    count = Column(Integer, nullable=False)
    last_updated = Column(DateTime(True), server_default=text("now()"))


class LabelTagRaw(Base):
    __tablename__ = 'label_tag_raw'

    label = Column(Integer, primary_key=True, nullable=False)
    editor = Column(Integer, primary_key=True, nullable=False, index=True)
    tag = Column(Integer, primary_key=True, nullable=False, index=True)
    is_upvote = Column(Boolean, nullable=False, server_default=text("true"))


class LabelType(Base):
    __tablename__ = 'label_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('label_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class Language(Base):
    __tablename__ = 'language'
    __table_args__ = (
        CheckConstraint('(iso_code_2t IS NOT NULL) OR (iso_code_3 IS NOT NULL)'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('language_id_seq'::regclass)"))
    iso_code_2t = Column(String(3), unique=True)
    iso_code_2b = Column(String(3), unique=True)
    iso_code_1 = Column(String(2), unique=True)
    name = Column(String(100), nullable=False)
    frequency = Column(Integer, nullable=False, server_default=text("0"))
    iso_code_3 = Column(String(3), unique=True)


class Link(Base):
    __tablename__ = 'link'
    __table_args__ = (
        CheckConstraint('(((end_date_year IS NOT NULL) OR (end_date_month IS NOT NULL) OR (end_date_day IS NOT NULL)) AND (ended = true)) OR ((end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL))'),
        Index('link_idx_type_attr', 'link_type', 'attribute_count')
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('link_id_seq'::regclass)"))
    link_type = Column(Integer, nullable=False)
    begin_date_year = Column(SmallInteger)
    begin_date_month = Column(SmallInteger)
    begin_date_day = Column(SmallInteger)
    end_date_year = Column(SmallInteger)
    end_date_month = Column(SmallInteger)
    end_date_day = Column(SmallInteger)
    attribute_count = Column(Integer, nullable=False, server_default=text("0"))
    created = Column(DateTime(True), server_default=text("now()"))
    ended = Column(Boolean, nullable=False, server_default=text("false"))


class LinkAttribute(Base):
    __tablename__ = 'link_attribute'

    link = Column(Integer, primary_key=True, nullable=False)
    attribute_type = Column(Integer, primary_key=True, nullable=False)
    created = Column(DateTime(True), server_default=text("now()"))


class LinkAttributeCredit(Base):
    __tablename__ = 'link_attribute_credit'

    link = Column(Integer, primary_key=True, nullable=False)
    attribute_type = Column(Integer, primary_key=True, nullable=False)
    credited_as = Column(Text, nullable=False)


class LinkAttributeTextValue(Base):
    __tablename__ = 'link_attribute_text_value'

    link = Column(Integer, primary_key=True, nullable=False)
    attribute_type = Column(Integer, primary_key=True, nullable=False)
    text_value = Column(Text, nullable=False)


class LinkAttributeType(Base):
    __tablename__ = 'link_attribute_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('link_attribute_type_id_seq'::regclass)"))
    parent = Column(Integer)
    root = Column(Integer, nullable=False)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    gid = Column(UUID, nullable=False, unique=True)
    name = Column(String(255), nullable=False)
    description = Column(Text)
    last_updated = Column(DateTime(True), server_default=text("now()"))


class LinkCreditableAttributeType(Base):
    __tablename__ = 'link_creditable_attribute_type'

    attribute_type = Column(Integer, primary_key=True)


class LinkTextAttributeType(Base):
    __tablename__ = 'link_text_attribute_type'

    attribute_type = Column(Integer, primary_key=True)


class LinkType(Base):
    __tablename__ = 'link_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('link_type_id_seq'::regclass)"))
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    gid = Column(UUID, nullable=False, unique=True)
    entity_type0 = Column(String(50), nullable=False)
    entity_type1 = Column(String(50), nullable=False)
    name = Column(String(255), nullable=False)
    description = Column(Text)
    link_phrase = Column(String(255), nullable=False)
    reverse_link_phrase = Column(String(255), nullable=False)
    long_link_phrase = Column(String(255), nullable=False)
    priority = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    is_deprecated = Column(Boolean, nullable=False, server_default=text("false"))
    has_dates = Column(Boolean, nullable=False, server_default=text("true"))
    entity0_cardinality = Column(Integer, nullable=False, server_default=text("0"))
    entity1_cardinality = Column(Integer, nullable=False, server_default=text("0"))


class LinkTypeAttributeType(Base):
    __tablename__ = 'link_type_attribute_type'

    link_type = Column(Integer, primary_key=True, nullable=False)
    attribute_type = Column(Integer, primary_key=True, nullable=False)
    min = Column(SmallInteger)
    max = Column(SmallInteger)
    last_updated = Column(DateTime(True), server_default=text("now()"))


class Medium(Base):
    __tablename__ = 'medium'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        Index('medium_idx_release_position', 'release', 'position')
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('medium_id_seq'::regclass)"))
    release = Column(Integer, nullable=False)
    position = Column(Integer, nullable=False)
    format = Column(Integer)
    name = Column(String, nullable=False, server_default=text("''::character varying"))
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    track_count = Column(Integer, nullable=False, index=True, server_default=text("0"))


class MediumAttribute(Base):
    __tablename__ = 'medium_attribute'
    __table_args__ = (
        CheckConstraint('((medium_attribute_type_allowed_value IS NULL) AND (medium_attribute_text IS NOT NULL)) OR ((medium_attribute_type_allowed_value IS NOT NULL) AND (medium_attribute_text IS NULL))'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('medium_attribute_id_seq'::regclass)"))
    medium = Column(Integer, nullable=False, index=True)
    medium_attribute_type = Column(Integer, nullable=False)
    medium_attribute_type_allowed_value = Column(Integer)
    medium_attribute_text = Column(Text)


class MediumAttributeType(Base):
    __tablename__ = 'medium_attribute_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('medium_attribute_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))
    free_text = Column(Boolean, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class MediumAttributeTypeAllowedFormat(Base):
    __tablename__ = 'medium_attribute_type_allowed_format'

    medium_format = Column(Integer, primary_key=True, nullable=False)
    medium_attribute_type = Column(Integer, primary_key=True, nullable=False)


class MediumAttributeTypeAllowedValue(Base):
    __tablename__ = 'medium_attribute_type_allowed_value'

    id = Column(Integer, primary_key=True, server_default=text("nextval('medium_attribute_type_allowed_value_id_seq'::regclass)"))
    medium_attribute_type = Column(Integer, nullable=False, index=True)
    value = Column(Text)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class MediumAttributeTypeAllowedValueAllowedFormat(Base):
    __tablename__ = 'medium_attribute_type_allowed_value_allowed_format'

    medium_format = Column(Integer, primary_key=True, nullable=False)
    medium_attribute_type_allowed_value = Column(Integer, primary_key=True, nullable=False)


class MediumCdtoc(Base):
    __tablename__ = 'medium_cdtoc'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
        Index('medium_cdtoc_idx_uniq', 'medium', 'cdtoc', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('medium_cdtoc_id_seq'::regclass)"))
    medium = Column(Integer, nullable=False, index=True)
    cdtoc = Column(Integer, nullable=False, index=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))


class MediumFormat(Base):
    __tablename__ = 'medium_format'

    id = Column(Integer, primary_key=True, server_default=text("nextval('medium_format_id_seq'::regclass)"))
    name = Column(String(100), nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    year = Column(SmallInteger)
    has_discids = Column(Boolean, nullable=False, server_default=text("false"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class MediumIndex(Base):
    __tablename__ = 'medium_index'

    medium = Column(Integer, primary_key=True)
    toc = Column(NullType, index=True)


t_old_editor_name = Table(
    'old_editor_name', metadata,
    Column('name', String(64), nullable=False)
)


class OrderableLinkType(Base):
    __tablename__ = 'orderable_link_type'
    __table_args__ = (
        CheckConstraint('(direction = 1) OR (direction = 2)'),
    )

    link_type = Column(Integer, primary_key=True)
    direction = Column(SmallInteger, nullable=False, server_default=text("1"))


class Place(Base):
    __tablename__ = 'place'
    __table_args__ = (
        CheckConstraint('(((end_date_year IS NOT NULL) OR (end_date_month IS NOT NULL) OR (end_date_day IS NOT NULL)) AND (ended = true)) OR ((end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL))'),
        CheckConstraint('edits_pending >= 0')
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('place_id_seq'::regclass)"))
    gid = Column(UUID, nullable=False, unique=True)
    name = Column(String, nullable=False, index=True)
    type = Column(Integer)
    address = Column(String, nullable=False, server_default=text("''::character varying"))
    area = Column(Integer, index=True)
    coordinates = Column(NullType)
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    begin_date_year = Column(SmallInteger)
    begin_date_month = Column(SmallInteger)
    begin_date_day = Column(SmallInteger)
    end_date_year = Column(SmallInteger)
    end_date_month = Column(SmallInteger)
    end_date_day = Column(SmallInteger)
    ended = Column(Boolean, nullable=False, server_default=text("false"))


class PlaceAlia(Base):
    __tablename__ = 'place_alias'
    __table_args__ = (
        CheckConstraint('(((end_date_year IS NOT NULL) OR (end_date_month IS NOT NULL) OR (end_date_day IS NOT NULL)) AND (ended = true)) OR ((end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL))'),
        CheckConstraint('((locale IS NULL) AND (primary_for_locale IS FALSE)) OR (locale IS NOT NULL)'),
        CheckConstraint('(type <> 2) OR ((type = 2) AND ((sort_name)::text = (name)::text) AND (begin_date_year IS NULL) AND (begin_date_month IS NULL) AND (begin_date_day IS NULL) AND (end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL) AND (primary_for_locale IS FALSE) AND (locale IS NULL))'),
        CheckConstraint('edits_pending >= 0'),
        Index('place_alias_idx_primary', 'place', 'locale', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('place_alias_id_seq'::regclass)"))
    place = Column(Integer, nullable=False, index=True)
    name = Column(String, nullable=False)
    locale = Column(Text)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    type = Column(Integer)
    sort_name = Column(String, nullable=False)
    begin_date_year = Column(SmallInteger)
    begin_date_month = Column(SmallInteger)
    begin_date_day = Column(SmallInteger)
    end_date_year = Column(SmallInteger)
    end_date_month = Column(SmallInteger)
    end_date_day = Column(SmallInteger)
    primary_for_locale = Column(Boolean, nullable=False, server_default=text("false"))
    ended = Column(Boolean, nullable=False, server_default=text("false"))


class PlaceAliasType(Base):
    __tablename__ = 'place_alias_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('place_alias_type_id_seq'::regclass)"))
    name = Column(Text, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class PlaceAnnotation(Base):
    __tablename__ = 'place_annotation'

    place = Column(Integer, primary_key=True, nullable=False)
    annotation = Column(Integer, primary_key=True, nullable=False)


class PlaceAttribute(Base):
    __tablename__ = 'place_attribute'
    __table_args__ = (
        CheckConstraint('((place_attribute_type_allowed_value IS NULL) AND (place_attribute_text IS NOT NULL)) OR ((place_attribute_type_allowed_value IS NOT NULL) AND (place_attribute_text IS NULL))'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('place_attribute_id_seq'::regclass)"))
    place = Column(Integer, nullable=False, index=True)
    place_attribute_type = Column(Integer, nullable=False)
    place_attribute_type_allowed_value = Column(Integer)
    place_attribute_text = Column(Text)


class PlaceAttributeType(Base):
    __tablename__ = 'place_attribute_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('place_attribute_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))
    free_text = Column(Boolean, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class PlaceAttributeTypeAllowedValue(Base):
    __tablename__ = 'place_attribute_type_allowed_value'

    id = Column(Integer, primary_key=True, server_default=text("nextval('place_attribute_type_allowed_value_id_seq'::regclass)"))
    place_attribute_type = Column(Integer, nullable=False, index=True)
    value = Column(Text)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class PlaceGidRedirect(Base):
    __tablename__ = 'place_gid_redirect'

    gid = Column(UUID, primary_key=True)
    new_id = Column(Integer, nullable=False, index=True)
    created = Column(DateTime(True), server_default=text("now()"))


class PlaceTag(Base):
    __tablename__ = 'place_tag'

    place = Column(Integer, primary_key=True, nullable=False)
    tag = Column(Integer, primary_key=True, nullable=False, index=True)
    count = Column(Integer, nullable=False)
    last_updated = Column(DateTime(True), server_default=text("now()"))


class PlaceTagRaw(Base):
    __tablename__ = 'place_tag_raw'

    place = Column(Integer, primary_key=True, nullable=False)
    editor = Column(Integer, primary_key=True, nullable=False, index=True)
    tag = Column(Integer, primary_key=True, nullable=False, index=True)
    is_upvote = Column(Boolean, nullable=False, server_default=text("true"))


class PlaceType(Base):
    __tablename__ = 'place_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('place_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class Recording(Base):
    __tablename__ = 'recording'
    __table_args__ = (
        CheckConstraint('(length IS NULL) OR (length > 0)'),
        CheckConstraint('edits_pending >= 0')
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('recording_id_seq'::regclass)"))
    gid = Column(UUID, nullable=False, unique=True)
    name = Column(String, nullable=False, index=True)
    artist_credit = Column(Integer, nullable=False, index=True)
    length = Column(Integer)
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    video = Column(Boolean, nullable=False, server_default=text("false"))


class RecordingAlia(Base):
    __tablename__ = 'recording_alias'
    __table_args__ = (
        CheckConstraint('(((end_date_year IS NOT NULL) OR (end_date_month IS NOT NULL) OR (end_date_day IS NOT NULL)) AND (ended = true)) OR ((end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL))'),
        CheckConstraint('((locale IS NULL) AND (primary_for_locale IS FALSE)) OR (locale IS NOT NULL)'),
        CheckConstraint('edits_pending >= 0'),
        Index('recording_alias_idx_primary', 'recording', 'locale', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('recording_alias_id_seq'::regclass)"))
    recording = Column(Integer, nullable=False, index=True)
    name = Column(String, nullable=False)
    locale = Column(Text)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    type = Column(Integer)
    sort_name = Column(String, nullable=False)
    begin_date_year = Column(SmallInteger)
    begin_date_month = Column(SmallInteger)
    begin_date_day = Column(SmallInteger)
    end_date_year = Column(SmallInteger)
    end_date_month = Column(SmallInteger)
    end_date_day = Column(SmallInteger)
    primary_for_locale = Column(Boolean, nullable=False, server_default=text("false"))
    ended = Column(Boolean, nullable=False, server_default=text("false"))


class RecordingAliasType(Base):
    __tablename__ = 'recording_alias_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('recording_alias_type_id_seq'::regclass)"))
    name = Column(Text, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class RecordingAnnotation(Base):
    __tablename__ = 'recording_annotation'

    recording = Column(Integer, primary_key=True, nullable=False)
    annotation = Column(Integer, primary_key=True, nullable=False)


class RecordingAttribute(Base):
    __tablename__ = 'recording_attribute'
    __table_args__ = (
        CheckConstraint('((recording_attribute_type_allowed_value IS NULL) AND (recording_attribute_text IS NOT NULL)) OR ((recording_attribute_type_allowed_value IS NOT NULL) AND (recording_attribute_text IS NULL))'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('recording_attribute_id_seq'::regclass)"))
    recording = Column(Integer, nullable=False, index=True)
    recording_attribute_type = Column(Integer, nullable=False)
    recording_attribute_type_allowed_value = Column(Integer)
    recording_attribute_text = Column(Text)


class RecordingAttributeType(Base):
    __tablename__ = 'recording_attribute_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('recording_attribute_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))
    free_text = Column(Boolean, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class RecordingAttributeTypeAllowedValue(Base):
    __tablename__ = 'recording_attribute_type_allowed_value'

    id = Column(Integer, primary_key=True, server_default=text("nextval('recording_attribute_type_allowed_value_id_seq'::regclass)"))
    recording_attribute_type = Column(Integer, nullable=False, index=True)
    value = Column(Text)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class RecordingGidRedirect(Base):
    __tablename__ = 'recording_gid_redirect'

    gid = Column(UUID, primary_key=True)
    new_id = Column(Integer, nullable=False, index=True)
    created = Column(DateTime(True), server_default=text("now()"))


class RecordingMeta(Base):
    __tablename__ = 'recording_meta'
    __table_args__ = (
        CheckConstraint('(rating >= 0) AND (rating <= 100)'),
    )

    id = Column(Integer, primary_key=True)
    rating = Column(SmallInteger)
    rating_count = Column(Integer)


class RecordingRatingRaw(Base):
    __tablename__ = 'recording_rating_raw'
    __table_args__ = (
        CheckConstraint('(rating >= 0) AND (rating <= 100)'),
    )

    recording = Column(Integer, primary_key=True, nullable=False)
    editor = Column(Integer, primary_key=True, nullable=False, index=True)
    rating = Column(SmallInteger, nullable=False)


t_recording_series = Table(
    'recording_series', metadata,
    Column('recording', Integer),
    Column('series', Integer),
    Column('relationship', Integer),
    Column('link_order', Integer),
    Column('link', Integer),
    Column('text_value', Text)
)


class RecordingTag(Base):
    __tablename__ = 'recording_tag'

    recording = Column(Integer, primary_key=True, nullable=False)
    tag = Column(Integer, primary_key=True, nullable=False, index=True)
    count = Column(Integer, nullable=False)
    last_updated = Column(DateTime(True), server_default=text("now()"))


class RecordingTagRaw(Base):
    __tablename__ = 'recording_tag_raw'

    recording = Column(Integer, primary_key=True, nullable=False, index=True)
    editor = Column(Integer, primary_key=True, nullable=False, index=True)
    tag = Column(Integer, primary_key=True, nullable=False, index=True)
    is_upvote = Column(Boolean, nullable=False, server_default=text("true"))


class Release(Base):
    __tablename__ = 'release'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('release_id_seq'::regclass)"))
    gid = Column(UUID, nullable=False, unique=True)
    name = Column(String, nullable=False, index=True)
    artist_credit = Column(Integer, nullable=False, index=True)
    release_group = Column(Integer, nullable=False, index=True)
    status = Column(Integer)
    packaging = Column(Integer)
    language = Column(Integer)
    script = Column(Integer)
    barcode = Column(String(255))
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    quality = Column(SmallInteger, nullable=False, server_default=text("'-1'::integer"))
    last_updated = Column(DateTime(True), server_default=text("now()"))


class ReleaseAlia(Base):
    __tablename__ = 'release_alias'
    __table_args__ = (
        CheckConstraint('(((end_date_year IS NOT NULL) OR (end_date_month IS NOT NULL) OR (end_date_day IS NOT NULL)) AND (ended = true)) OR ((end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL))'),
        CheckConstraint('((locale IS NULL) AND (primary_for_locale IS FALSE)) OR (locale IS NOT NULL)'),
        CheckConstraint('edits_pending >= 0'),
        Index('release_alias_idx_primary', 'release', 'locale', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('release_alias_id_seq'::regclass)"))
    release = Column(Integer, nullable=False, index=True)
    name = Column(String, nullable=False)
    locale = Column(Text)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    type = Column(Integer)
    sort_name = Column(String, nullable=False)
    begin_date_year = Column(SmallInteger)
    begin_date_month = Column(SmallInteger)
    begin_date_day = Column(SmallInteger)
    end_date_year = Column(SmallInteger)
    end_date_month = Column(SmallInteger)
    end_date_day = Column(SmallInteger)
    primary_for_locale = Column(Boolean, nullable=False, server_default=text("false"))
    ended = Column(Boolean, nullable=False, server_default=text("false"))


class ReleaseAliasType(Base):
    __tablename__ = 'release_alias_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('release_alias_type_id_seq'::regclass)"))
    name = Column(Text, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False)


class ReleaseAnnotation(Base):
    __tablename__ = 'release_annotation'

    release = Column(Integer, primary_key=True, nullable=False)
    annotation = Column(Integer, primary_key=True, nullable=False)


class ReleaseAttribute(Base):
    __tablename__ = 'release_attribute'
    __table_args__ = (
        CheckConstraint('((release_attribute_type_allowed_value IS NULL) AND (release_attribute_text IS NOT NULL)) OR ((release_attribute_type_allowed_value IS NOT NULL) AND (release_attribute_text IS NULL))'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('release_attribute_id_seq'::regclass)"))
    release = Column(Integer, nullable=False, index=True)
    release_attribute_type = Column(Integer, nullable=False)
    release_attribute_type_allowed_value = Column(Integer)
    release_attribute_text = Column(Text)


class ReleaseAttributeType(Base):
    __tablename__ = 'release_attribute_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('release_attribute_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))
    free_text = Column(Boolean, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class ReleaseAttributeTypeAllowedValue(Base):
    __tablename__ = 'release_attribute_type_allowed_value'

    id = Column(Integer, primary_key=True, server_default=text("nextval('release_attribute_type_allowed_value_id_seq'::regclass)"))
    release_attribute_type = Column(Integer, nullable=False, index=True)
    value = Column(Text)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class ReleaseCountry(Base):
    __tablename__ = 'release_country'

    release = Column(Integer, primary_key=True, nullable=False)
    country = Column(Integer, primary_key=True, nullable=False, index=True)
    date_year = Column(SmallInteger)
    date_month = Column(SmallInteger)
    date_day = Column(SmallInteger)


class ReleaseCoverart(Base):
    __tablename__ = 'release_coverart'

    id = Column(Integer, primary_key=True)
    last_updated = Column(DateTime(True))
    cover_art_url = Column(String(255))


t_release_event = Table(
    'release_event', metadata,
    Column('release', Integer),
    Column('date_year', SmallInteger),
    Column('date_month', SmallInteger),
    Column('date_day', SmallInteger),
    Column('country', Integer)
)


class ReleaseGidRedirect(Base):
    __tablename__ = 'release_gid_redirect'

    gid = Column(UUID, primary_key=True)
    new_id = Column(Integer, nullable=False, index=True)
    created = Column(DateTime(True), server_default=text("now()"))


class ReleaseGroup(Base):
    __tablename__ = 'release_group'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('release_group_id_seq'::regclass)"))
    gid = Column(UUID, nullable=False, unique=True)
    name = Column(String, nullable=False, index=True)
    artist_credit = Column(Integer, nullable=False, index=True)
    type = Column(Integer)
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))


class ReleaseGroupAlia(Base):
    __tablename__ = 'release_group_alias'
    __table_args__ = (
        CheckConstraint('(((end_date_year IS NOT NULL) OR (end_date_month IS NOT NULL) OR (end_date_day IS NOT NULL)) AND (ended = true)) OR ((end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL))'),
        CheckConstraint('((locale IS NULL) AND (primary_for_locale IS FALSE)) OR (locale IS NOT NULL)'),
        CheckConstraint('edits_pending >= 0'),
        Index('release_group_alias_idx_primary', 'release_group', 'locale', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('release_group_alias_id_seq'::regclass)"))
    release_group = Column(Integer, nullable=False, index=True)
    name = Column(String, nullable=False)
    locale = Column(Text)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    type = Column(Integer)
    sort_name = Column(String, nullable=False)
    begin_date_year = Column(SmallInteger)
    begin_date_month = Column(SmallInteger)
    begin_date_day = Column(SmallInteger)
    end_date_year = Column(SmallInteger)
    end_date_month = Column(SmallInteger)
    end_date_day = Column(SmallInteger)
    primary_for_locale = Column(Boolean, nullable=False, server_default=text("false"))
    ended = Column(Boolean, nullable=False, server_default=text("false"))


class ReleaseGroupAliasType(Base):
    __tablename__ = 'release_group_alias_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('release_group_alias_type_id_seq'::regclass)"))
    name = Column(Text, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class ReleaseGroupAnnotation(Base):
    __tablename__ = 'release_group_annotation'

    release_group = Column(Integer, primary_key=True, nullable=False)
    annotation = Column(Integer, primary_key=True, nullable=False)


class ReleaseGroupAttribute(Base):
    __tablename__ = 'release_group_attribute'
    __table_args__ = (
        CheckConstraint('((release_group_attribute_type_allowed_value IS NULL) AND (release_group_attribute_text IS NOT NULL)) OR ((release_group_attribute_type_allowed_value IS NOT NULL) AND (release_group_attribute_text IS NULL))'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('release_group_attribute_id_seq'::regclass)"))
    release_group = Column(Integer, nullable=False, index=True)
    release_group_attribute_type = Column(Integer, nullable=False)
    release_group_attribute_type_allowed_value = Column(Integer)
    release_group_attribute_text = Column(Text)


class ReleaseGroupAttributeType(Base):
    __tablename__ = 'release_group_attribute_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('release_group_attribute_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))
    free_text = Column(Boolean, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class ReleaseGroupAttributeTypeAllowedValue(Base):
    __tablename__ = 'release_group_attribute_type_allowed_value'

    id = Column(Integer, primary_key=True, server_default=text("nextval('release_group_attribute_type_allowed_value_id_seq'::regclass)"))
    release_group_attribute_type = Column(Integer, nullable=False, index=True)
    value = Column(Text)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class ReleaseGroupGidRedirect(Base):
    __tablename__ = 'release_group_gid_redirect'

    gid = Column(UUID, primary_key=True)
    new_id = Column(Integer, nullable=False, index=True)
    created = Column(DateTime(True), server_default=text("now()"))


class ReleaseGroupMeta(Base):
    __tablename__ = 'release_group_meta'
    __table_args__ = (
        CheckConstraint('(rating >= 0) AND (rating <= 100)'),
    )

    id = Column(Integer, primary_key=True)
    release_count = Column(Integer, nullable=False, server_default=text("0"))
    first_release_date_year = Column(SmallInteger)
    first_release_date_month = Column(SmallInteger)
    first_release_date_day = Column(SmallInteger)
    rating = Column(SmallInteger)
    rating_count = Column(Integer)


class ReleaseGroupPrimaryType(Base):
    __tablename__ = 'release_group_primary_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('release_group_primary_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class ReleaseGroupRatingRaw(Base):
    __tablename__ = 'release_group_rating_raw'
    __table_args__ = (
        CheckConstraint('(rating >= 0) AND (rating <= 100)'),
    )

    release_group = Column(Integer, primary_key=True, nullable=False, index=True)
    editor = Column(Integer, primary_key=True, nullable=False, index=True)
    rating = Column(SmallInteger, nullable=False)


class ReleaseGroupSecondaryType(Base):
    __tablename__ = 'release_group_secondary_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('release_group_secondary_type_id_seq'::regclass)"))
    name = Column(Text, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class ReleaseGroupSecondaryTypeJoin(Base):
    __tablename__ = 'release_group_secondary_type_join'

    release_group = Column(Integer, primary_key=True, nullable=False)
    secondary_type = Column(Integer, primary_key=True, nullable=False)
    created = Column(DateTime(True), nullable=False, server_default=text("now()"))


t_release_group_series = Table(
    'release_group_series', metadata,
    Column('release_group', Integer),
    Column('series', Integer),
    Column('relationship', Integer),
    Column('link_order', Integer),
    Column('link', Integer),
    Column('text_value', Text)
)


class ReleaseGroupTag(Base):
    __tablename__ = 'release_group_tag'

    release_group = Column(Integer, primary_key=True, nullable=False)
    tag = Column(Integer, primary_key=True, nullable=False, index=True)
    count = Column(Integer, nullable=False)
    last_updated = Column(DateTime(True), server_default=text("now()"))


class ReleaseGroupTagRaw(Base):
    __tablename__ = 'release_group_tag_raw'

    release_group = Column(Integer, primary_key=True, nullable=False)
    editor = Column(Integer, primary_key=True, nullable=False, index=True)
    tag = Column(Integer, primary_key=True, nullable=False, index=True)
    is_upvote = Column(Boolean, nullable=False, server_default=text("true"))


class ReleaseLabel(Base):
    __tablename__ = 'release_label'

    id = Column(Integer, primary_key=True, server_default=text("nextval('release_label_id_seq'::regclass)"))
    release = Column(Integer, nullable=False, index=True)
    label = Column(Integer, index=True)
    catalog_number = Column(String(255))
    last_updated = Column(DateTime(True), server_default=text("now()"))


class ReleaseMeta(Base):
    __tablename__ = 'release_meta'

    id = Column(Integer, primary_key=True)
    date_added = Column(DateTime(True), server_default=text("now()"))
    info_url = Column(String(255))
    amazon_asin = Column(String(10))
    amazon_store = Column(String(20))
    cover_art_presence = Column(Enum('absent', 'present', 'darkened', name='cover_art_presence'), nullable=False, server_default=text("'absent'::cover_art_presence"))


class ReleasePackaging(Base):
    __tablename__ = 'release_packaging'

    id = Column(Integer, primary_key=True, server_default=text("nextval('release_packaging_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class ReleaseRaw(Base):
    __tablename__ = 'release_raw'

    id = Column(Integer, primary_key=True, server_default=text("nextval('release_raw_id_seq'::regclass)"))
    title = Column(String(255), nullable=False)
    artist = Column(String(255))
    added = Column(DateTime(True), server_default=text("now()"))
    last_modified = Column(DateTime(True), index=True, server_default=text("now()"))
    lookup_count = Column(Integer, index=True, server_default=text("0"))
    modify_count = Column(Integer, index=True, server_default=text("0"))
    source = Column(Integer, server_default=text("0"))
    barcode = Column(String(255))
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))


t_release_series = Table(
    'release_series', metadata,
    Column('release', Integer),
    Column('series', Integer),
    Column('relationship', Integer),
    Column('link_order', Integer),
    Column('link', Integer),
    Column('text_value', Text)
)


class ReleaseStatu(Base):
    __tablename__ = 'release_status'

    id = Column(Integer, primary_key=True, server_default=text("nextval('release_status_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class ReleaseTag(Base):
    __tablename__ = 'release_tag'

    release = Column(Integer, primary_key=True, nullable=False)
    tag = Column(Integer, primary_key=True, nullable=False, index=True)
    count = Column(Integer, nullable=False)
    last_updated = Column(DateTime(True), server_default=text("now()"))


class ReleaseTagRaw(Base):
    __tablename__ = 'release_tag_raw'

    release = Column(Integer, primary_key=True, nullable=False)
    editor = Column(Integer, primary_key=True, nullable=False, index=True)
    tag = Column(Integer, primary_key=True, nullable=False, index=True)
    is_upvote = Column(Boolean, nullable=False, server_default=text("true"))


class ReleaseUnknownCountry(Base):
    __tablename__ = 'release_unknown_country'

    release = Column(Integer, primary_key=True)
    date_year = Column(SmallInteger)
    date_month = Column(SmallInteger)
    date_day = Column(SmallInteger)


class ReplicationControl(Base):
    __tablename__ = 'replication_control'

    id = Column(Integer, primary_key=True, server_default=text("nextval('replication_control_id_seq'::regclass)"))
    current_schema_sequence = Column(Integer, nullable=False)
    current_replication_sequence = Column(Integer)
    last_replication_date = Column(DateTime(True))


class Script(Base):
    __tablename__ = 'script'

    id = Column(Integer, primary_key=True, server_default=text("nextval('script_id_seq'::regclass)"))
    iso_code = Column(String(4), nullable=False, unique=True)
    iso_number = Column(String(3), nullable=False)
    name = Column(String(100), nullable=False)
    frequency = Column(Integer, nullable=False, server_default=text("0"))


class Series(Base):
    __tablename__ = 'series'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('series_id_seq'::regclass)"))
    gid = Column(UUID, nullable=False, unique=True)
    name = Column(String, nullable=False, index=True)
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))
    type = Column(Integer, nullable=False)
    ordering_attribute = Column(Integer, nullable=False)
    ordering_type = Column(Integer, nullable=False)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))


class SeriesAlia(Base):
    __tablename__ = 'series_alias'
    __table_args__ = (
        CheckConstraint('(((end_date_year IS NOT NULL) OR (end_date_month IS NOT NULL) OR (end_date_day IS NOT NULL)) AND (ended = true)) OR ((end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL))'),
        CheckConstraint('((locale IS NULL) AND (primary_for_locale IS FALSE)) OR (locale IS NOT NULL)'),
        CheckConstraint('(type <> 2) OR ((type = 2) AND ((sort_name)::text = (name)::text) AND (begin_date_year IS NULL) AND (begin_date_month IS NULL) AND (begin_date_day IS NULL) AND (end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL) AND (primary_for_locale IS FALSE) AND (locale IS NULL))'),
        CheckConstraint('edits_pending >= 0'),
        Index('series_alias_idx_primary', 'series', 'locale', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('series_alias_id_seq'::regclass)"))
    series = Column(Integer, nullable=False, index=True)
    name = Column(String, nullable=False)
    locale = Column(Text)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    type = Column(Integer)
    sort_name = Column(String, nullable=False)
    begin_date_year = Column(SmallInteger)
    begin_date_month = Column(SmallInteger)
    begin_date_day = Column(SmallInteger)
    end_date_year = Column(SmallInteger)
    end_date_month = Column(SmallInteger)
    end_date_day = Column(SmallInteger)
    primary_for_locale = Column(Boolean, nullable=False, server_default=text("false"))
    ended = Column(Boolean, nullable=False, server_default=text("false"))


class SeriesAliasType(Base):
    __tablename__ = 'series_alias_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('series_alias_type_id_seq'::regclass)"))
    name = Column(Text, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class SeriesAnnotation(Base):
    __tablename__ = 'series_annotation'

    series = Column(Integer, primary_key=True, nullable=False)
    annotation = Column(Integer, primary_key=True, nullable=False)


class SeriesAttribute(Base):
    __tablename__ = 'series_attribute'
    __table_args__ = (
        CheckConstraint('((series_attribute_type_allowed_value IS NULL) AND (series_attribute_text IS NOT NULL)) OR ((series_attribute_type_allowed_value IS NOT NULL) AND (series_attribute_text IS NULL))'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('series_attribute_id_seq'::regclass)"))
    series = Column(Integer, nullable=False, index=True)
    series_attribute_type = Column(Integer, nullable=False)
    series_attribute_type_allowed_value = Column(Integer)
    series_attribute_text = Column(Text)


class SeriesAttributeType(Base):
    __tablename__ = 'series_attribute_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('series_attribute_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))
    free_text = Column(Boolean, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class SeriesAttributeTypeAllowedValue(Base):
    __tablename__ = 'series_attribute_type_allowed_value'

    id = Column(Integer, primary_key=True, server_default=text("nextval('series_attribute_type_allowed_value_id_seq'::regclass)"))
    series_attribute_type = Column(Integer, nullable=False, index=True)
    value = Column(Text)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class SeriesGidRedirect(Base):
    __tablename__ = 'series_gid_redirect'

    gid = Column(UUID, primary_key=True)
    new_id = Column(Integer, nullable=False, index=True)
    created = Column(DateTime(True), server_default=text("now()"))


class SeriesOrderingType(Base):
    __tablename__ = 'series_ordering_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('series_ordering_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class SeriesTag(Base):
    __tablename__ = 'series_tag'

    series = Column(Integer, primary_key=True, nullable=False)
    tag = Column(Integer, primary_key=True, nullable=False, index=True)
    count = Column(Integer, nullable=False)
    last_updated = Column(DateTime(True), server_default=text("now()"))


class SeriesTagRaw(Base):
    __tablename__ = 'series_tag_raw'

    series = Column(Integer, primary_key=True, nullable=False, index=True)
    editor = Column(Integer, primary_key=True, nullable=False, index=True)
    tag = Column(Integer, primary_key=True, nullable=False, index=True)
    is_upvote = Column(Boolean, nullable=False, server_default=text("true"))


class SeriesType(Base):
    __tablename__ = 'series_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('series_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    entity_type = Column(String(50), nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class Tag(Base):
    __tablename__ = 'tag'

    id = Column(Integer, primary_key=True, server_default=text("nextval('tag_id_seq'::regclass)"))
    name = Column(String(255), nullable=False, unique=True)
    ref_count = Column(Integer, nullable=False, server_default=text("0"))


class TagRelation(Base):
    __tablename__ = 'tag_relation'
    __table_args__ = (
        CheckConstraint('tag1 < tag2'),
    )

    tag1 = Column(Integer, primary_key=True, nullable=False)
    tag2 = Column(Integer, primary_key=True, nullable=False)
    weight = Column(Integer, nullable=False)
    last_updated = Column(DateTime(True), server_default=text("now()"))


class Track(Base):
    __tablename__ = 'track'
    __table_args__ = (
        CheckConstraint('(length IS NULL) OR (length > 0)'),
        CheckConstraint('edits_pending >= 0'),
        Index('track_idx_medium_position', 'medium', 'position')
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('track_id_seq'::regclass)"))
    gid = Column(UUID, nullable=False, unique=True)
    recording = Column(Integer, nullable=False, index=True)
    medium = Column(Integer, nullable=False)
    position = Column(Integer, nullable=False)
    number = Column(Text, nullable=False)
    name = Column(String, nullable=False, index=True)
    artist_credit = Column(Integer, nullable=False, index=True)
    length = Column(Integer)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    is_data_track = Column(Boolean, nullable=False, server_default=text("false"))


class TrackGidRedirect(Base):
    __tablename__ = 'track_gid_redirect'

    gid = Column(UUID, primary_key=True)
    new_id = Column(Integer, nullable=False, index=True)
    created = Column(DateTime(True), server_default=text("now()"))


class TrackRaw(Base):
    __tablename__ = 'track_raw'

    id = Column(Integer, primary_key=True, server_default=text("nextval('track_raw_id_seq'::regclass)"))
    release = Column(Integer, nullable=False, index=True)
    title = Column(String(255), nullable=False)
    artist = Column(String(255))
    sequence = Column(Integer, nullable=False)


class Url(Base):
    __tablename__ = 'url'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('url_id_seq'::regclass)"))
    gid = Column(UUID, nullable=False, unique=True)
    url = Column(Text, nullable=False, unique=True)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))


class UrlGidRedirect(Base):
    __tablename__ = 'url_gid_redirect'

    gid = Column(UUID, primary_key=True)
    new_id = Column(Integer, nullable=False, index=True)
    created = Column(DateTime(True), server_default=text("now()"))


class Vote(Base):
    __tablename__ = 'vote'
    __table_args__ = (
        Index('vote_idx_editor_edit', 'editor', 'edit'),
        Index('vote_idx_editor_vote_time', 'editor', 'vote_time')
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('vote_id_seq'::regclass)"))
    editor = Column(Integer, nullable=False)
    edit = Column(Integer, nullable=False, index=True)
    vote = Column(SmallInteger, nullable=False)
    vote_time = Column(DateTime(True), index=True, server_default=text("now()"))
    superseded = Column(Boolean, nullable=False, server_default=text("false"))


class Work(Base):
    __tablename__ = 'work'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('work_id_seq'::regclass)"))
    gid = Column(UUID, nullable=False, unique=True)
    name = Column(String, nullable=False, index=True)
    type = Column(Integer)
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))


class WorkAlia(Base):
    __tablename__ = 'work_alias'
    __table_args__ = (
        CheckConstraint('(((end_date_year IS NOT NULL) OR (end_date_month IS NOT NULL) OR (end_date_day IS NOT NULL)) AND (ended = true)) OR ((end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL))'),
        CheckConstraint('((locale IS NULL) AND (primary_for_locale IS FALSE)) OR (locale IS NOT NULL)'),
        CheckConstraint('(type <> 2) OR ((type = 2) AND ((sort_name)::text = (name)::text) AND (begin_date_year IS NULL) AND (begin_date_month IS NULL) AND (begin_date_day IS NULL) AND (end_date_year IS NULL) AND (end_date_month IS NULL) AND (end_date_day IS NULL) AND (primary_for_locale IS FALSE) AND (locale IS NULL))'),
        CheckConstraint('edits_pending >= 0'),
        Index('work_alias_idx_primary', 'work', 'locale', unique=True)
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('work_alias_id_seq'::regclass)"))
    work = Column(Integer, nullable=False, index=True)
    name = Column(String, nullable=False)
    locale = Column(Text)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    last_updated = Column(DateTime(True), server_default=text("now()"))
    type = Column(Integer)
    sort_name = Column(String, nullable=False)
    begin_date_year = Column(SmallInteger)
    begin_date_month = Column(SmallInteger)
    begin_date_day = Column(SmallInteger)
    end_date_year = Column(SmallInteger)
    end_date_month = Column(SmallInteger)
    end_date_day = Column(SmallInteger)
    primary_for_locale = Column(Boolean, nullable=False, server_default=text("false"))
    ended = Column(Boolean, nullable=False, server_default=text("false"))


class WorkAliasType(Base):
    __tablename__ = 'work_alias_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('work_alias_type_id_seq'::regclass)"))
    name = Column(Text, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class WorkAnnotation(Base):
    __tablename__ = 'work_annotation'

    work = Column(Integer, primary_key=True, nullable=False)
    annotation = Column(Integer, primary_key=True, nullable=False)


class WorkAttribute(Base):
    __tablename__ = 'work_attribute'
    __table_args__ = (
        CheckConstraint('((work_attribute_type_allowed_value IS NULL) AND (work_attribute_text IS NOT NULL)) OR ((work_attribute_type_allowed_value IS NOT NULL) AND (work_attribute_text IS NULL))'),
    )

    id = Column(Integer, primary_key=True, server_default=text("nextval('work_attribute_id_seq'::regclass)"))
    work = Column(Integer, nullable=False, index=True)
    work_attribute_type = Column(Integer, nullable=False)
    work_attribute_type_allowed_value = Column(Integer)
    work_attribute_text = Column(Text)


class WorkAttributeType(Base):
    __tablename__ = 'work_attribute_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('work_attribute_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    comment = Column(String(255), nullable=False, server_default=text("''::character varying"))
    free_text = Column(Boolean, nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class WorkAttributeTypeAllowedValue(Base):
    __tablename__ = 'work_attribute_type_allowed_value'

    id = Column(Integer, primary_key=True, server_default=text("nextval('work_attribute_type_allowed_value_id_seq'::regclass)"))
    work_attribute_type = Column(Integer, nullable=False, index=True)
    value = Column(Text)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)


class WorkGidRedirect(Base):
    __tablename__ = 'work_gid_redirect'

    gid = Column(UUID, primary_key=True)
    new_id = Column(Integer, nullable=False, index=True)
    created = Column(DateTime(True), server_default=text("now()"))


class WorkLanguage(Base):
    __tablename__ = 'work_language'
    __table_args__ = (
        CheckConstraint('edits_pending >= 0'),
    )

    work = Column(Integer, primary_key=True, nullable=False)
    language = Column(Integer, primary_key=True, nullable=False)
    edits_pending = Column(Integer, nullable=False, server_default=text("0"))
    created = Column(DateTime(True), server_default=text("now()"))


class WorkMeta(Base):
    __tablename__ = 'work_meta'
    __table_args__ = (
        CheckConstraint('(rating >= 0) AND (rating <= 100)'),
    )

    id = Column(Integer, primary_key=True)
    rating = Column(SmallInteger)
    rating_count = Column(Integer)


class WorkRatingRaw(Base):
    __tablename__ = 'work_rating_raw'
    __table_args__ = (
        CheckConstraint('(rating >= 0) AND (rating <= 100)'),
    )

    work = Column(Integer, primary_key=True, nullable=False)
    editor = Column(Integer, primary_key=True, nullable=False)
    rating = Column(SmallInteger, nullable=False)


t_work_series = Table(
    'work_series', metadata,
    Column('work', Integer),
    Column('series', Integer),
    Column('relationship', Integer),
    Column('link_order', Integer),
    Column('link', Integer),
    Column('text_value', Text)
)


class WorkTag(Base):
    __tablename__ = 'work_tag'

    work = Column(Integer, primary_key=True, nullable=False)
    tag = Column(Integer, primary_key=True, nullable=False, index=True)
    count = Column(Integer, nullable=False)
    last_updated = Column(DateTime(True), server_default=text("now()"))


class WorkTagRaw(Base):
    __tablename__ = 'work_tag_raw'

    work = Column(Integer, primary_key=True, nullable=False)
    editor = Column(Integer, primary_key=True, nullable=False)
    tag = Column(Integer, primary_key=True, nullable=False, index=True)
    is_upvote = Column(Boolean, nullable=False, server_default=text("true"))


class WorkType(Base):
    __tablename__ = 'work_type'

    id = Column(Integer, primary_key=True, server_default=text("nextval('work_type_id_seq'::regclass)"))
    name = Column(String(255), nullable=False)
    parent = Column(Integer)
    child_order = Column(Integer, nullable=False, server_default=text("0"))
    description = Column(Text)
    gid = Column(UUID, nullable=False, unique=True)
